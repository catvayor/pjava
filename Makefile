pjava: $(wildcard src/*.java src/typ/*.java src/analyser/*.java src/syn/*.java src/global/*.java)
	javac -cp src -d target src/*.java src/typ/*.java src/analyser/*.java src/syn/*.java src/global/*.java
	echo '#!/bin/bash' > pjava
	echo 'P=$$(dirname $$0)' >> pjava
	echo 'java -cp $$P/target Main $$@' >> pjava
	chmod u+x pjava

tar: rapport.pdf
	tar -cvzf bailly.gz src rapport.tex Makefile readme.md rapport.pdf

rapport.pdf: rapport.tex
	pdflatex rapport.tex
	rm rapport.aux rapport.log

clean:
	rm -fr target 
	rm -f pjava
