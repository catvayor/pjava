# pjava

Compilateur Petit Java : Projet ENS langage de programmation et compilation 2021

Le rapport détail la sémantique du code, ce fichier traite du code en lui même.

Le compilateur est écrit en java, un fichier bash (créé a la compilation du projet) permet de ne pas devoir appelé la machine java à chaque utilisation, pour compiler le projet : `make` ou `make pjava` feront le nessessaire.

Contrairement à la première version envoyé, l'analyse lexical et syntaxique ne sont plus générer par les générateur `jflex` et `java-cup`, mais codé directement en java.

Le code se sépare en 3 package (plus un package `global`) : 

* le package `analyser` fourni le parser.
* le package `syn` est utilisé pour la representation de l'arbre *syn*taxique
* le package `typ` est utilisé pour l'arbre de *typ*age (construction et représentation)
* hors package, on a `Main.java` qui portent bien son nom et `Assemb.java` pour la production de code.
