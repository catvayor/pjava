import global.*;
import typ.*;
import java.util.Vector;
import java.util.Hashtable;
import java.util.HashSet;
import java.io.FileWriter;

public class Assemb{
    public FileWriter outfile;
    public TypEnvironment env;

    void write(boolean indent, String code) throws Exception{
        if(indent)
            outfile.write("\t" + code + "\n");
        else
            outfile.write(code + "\n");
    }
    void write(String code) throws Exception{
        write(true, code);
    }
    void writeLabel(String lab) throws Exception{
        write(false, lab + ":");
    }
    void writeCom(String msg) throws Exception{
        write(false, "#" + msg);
    }
    void writeVerb(String msg) throws Exception{
        if(verbose_out)
            writeCom(msg);
    //    System.out.println(msg);
    }

    void writeRet()throws Exception{ write("ret"); } 
    void writeCall(String pos)throws Exception{ write("call " + pos); }
    
    void writePush(String d)throws Exception{ write("pushq " + d); }
    void writePop(String reg)throws Exception{ write("popq " + reg); }
    
    void writeMov(String src, String dest)throws Exception{
        write("movq " + src + "," + dest);
    }
    void writeSet(String flag, String dest)throws Exception{
        write("set" + flag + " " + dest);
    }
    void writeLea(String src, String dest)throws Exception{
        write("leaq " + src + "," + dest);
    }

    
    void writeAdd(String src, String dest)throws Exception{
        write("addq " + src + "," + dest);
    }
    void writeSub(String src, String dest)throws Exception{
        write("subq " + src + "," + dest);
    }
    void writeMul(String src, String dest)throws Exception{
        write("imulq " + src + "," + dest);
    }
    void writeDiv(String div)throws Exception{
        write("cqto");
        write("idivq " + div);
    }
    void writeMod(String div)throws Exception{
        write("cqto");
        write("idivq " + div);
        writeMov("%rdx", "%rax");
    }

    void writeNeg(String val)throws Exception{
        write("negq " + val);
    }

    void writeAnd(String src, String dest)throws Exception{
        write("andq " + src + "," + dest);
    }
    void writeOr(String src, String dest)throws Exception{
        write("orq " + src + "," + dest);
    }
    void writeXor(String src, String dest)throws Exception{
        write("xorq " + src + "," + dest);
    }

    void writeCmp(String val1, String val2)throws Exception{
        write("cmpq " + val1 + "," + val2);
    }
    void writeTest(String val1, String val2)throws Exception{
        write("testq " + val1 + "," + val2);
    }

    void writeJmp(String addr)throws Exception{
        write("jmp " + addr);
    }
    void writeJcc(String flag, String addr)throws Exception{
        write("j" + flag + " " + addr);
    }

    int markCount;
    boolean verbose_out;

    HashSet<String> cl_done;
    Hashtable<String, Integer> meth_addr;
    Vector<String> meth_done;
    int meth_nb;

    int getMark(){
        int ret = markCount;
        markCount += 1;
        return ret;
    }
    int String_length(String str){
        int ret  = 0;
        for(int i = 0; i < str.length(); ++i){
            ++ret;
            if(str.charAt(i) == '\\')
                ++i;
        }
        return ret;
    }

    public static String loc_com(Location beg, Location end){
        return "(" + beg.line + "," + beg.col + ") to (" +
                end.line + "," + end.col + ")";
    }

    public Assemb(FileWriter out, TypEnvironment e, boolean verbose){
        outfile = out;
        env = e;
        verbose_out = verbose;
    }

    void writeCalc(TypExpr e1, TypExpr e2, TypScope scope)throws Exception{
        writeExpr(e1, scope);
        writePush("%rax");
        writeExpr(e2, scope);
        writeMov("%rax", "%rdi");
        writePop("%rax");
    }

    void writeComp(TypExpr e1, TypExpr e2, String flag, TypScope scope)throws Exception{
        writeCalc(e1, e2, scope);
        writeXor("%r8","%r8");
        writeCmp("%rdi","%rax");
        writeSet(flag,"%r8b");
        writeMov("%r8","%rax");
    }

    public void writeVarloc(TypExpr expr, TypScope scope)throws Exception{
        switch(expr.opType){
            case TypExpr.VAR:
                writeLea(scope.var_loc.get(expr.ident)+"(%rbp)","%rax");
                break;

            case TypExpr.CLVAR:
                writeExpr(expr.from, scope);
                writeLea(expr.from.type.getFieldLoc(expr.ident,env) + "(%rax)", "%rax");
                break;
                
            case TypExpr.STVAR:
                writeMov("$.static.var."+expr.from.type.ident+"."+expr.ident, "%rax");
                break;

            default:
                break;
        }
    }

    public void writeExpr(TypExpr expr, TypScope scope) throws Exception{
        writeVerb("beg expr " + loc_com(expr.beg, expr.end));
        switch(expr.opType){
            case TypExpr.NO_EXPR:
                writeXor("%rax","%rax");
                break;

            case TypExpr.LIT_I:
                writeMov("$" + expr.lit_i_val, "%rax");
                break;

            case TypExpr.LIT_B:
                writeMov("$" + (expr.lit_b_val?1:0), "%rax");
                break;

            case TypExpr.LIT_S:
                write(".data");
                writeLabel("0");
                write(".quad .desc.String");
                write(".quad " + String_length(expr.lit_s_val));
                write(".string \"" + expr.lit_s_val + "\"");
                write(".text");
                writeMov("$0b", "%rax");
                break;

            case TypExpr.THIS:
                writeMov("16(%rbp)", "%rax");
                break;

            case TypExpr.BUILD:
                for(TypExpr arg : expr.args){
                    writeExpr(arg, scope);
                    writePush("%rax");
                }
                writeCall(".build."+expr.type.ident);
                writePop("%rax");
                
                for(TypExpr arg : expr.args)
                    writePop("%rdi");
                break;

            case TypExpr.CALL:
                for(TypExpr arg : expr.args){
                    writeExpr(arg, scope);
                    writePush("%rax");
                }
                writeExpr(expr.from, scope);
                writePush("%rax");
                writeCall(".dyn."+expr.ident);
                writePop("%rdi");
                
                for(TypExpr arg : expr.args)
                    writePop("%rdi");
                break;
            
            case TypExpr.STCALL:
                for(TypExpr arg : expr.args){
                    writeExpr(arg, scope);
                    writePush("%rax");
                }
                
                writeCall(".static.method."+expr.from.type.ident+"."+expr.ident);
                
                for(TypExpr arg : expr.args)
                    writePop("%rdi");
                break;
            
            case TypExpr.VAR:
                writeMov(scope.var_loc.get(expr.ident)+"(%rbp)","%rax");
                break;

            case TypExpr.CLVAR:
                writeExpr(expr.from, scope);
                writeMov(expr.from.type.getFieldLoc(expr.ident, env) + "(%rax)", "%rax");
                break;
            
            case TypExpr.STVAR:
                writeMov(".static.var."+expr.from.type.ident+"."+expr.ident, "%rax");
                break;

            case TypExpr.NULL:
                writeXor("%rax","%rax");
                break;

            case TypExpr.ASIGN:
                writeVarloc(expr.e1, scope);
                writePush("%rax");
                writeExpr(expr.e2, scope);
                writePop("%rdi");
                writeMov("%rax","(%rdi)");
                //%rax already contains what is needed
                break;

            case TypExpr.NOT:
                writeExpr(expr.e1, scope);
                writeXor("$1","%rax");
                break;

            case TypExpr.UMIN:
                writeExpr(expr.e1, scope);
                writeNeg("%rax");
                break;

            case TypExpr.OP:{
                if(expr.type.isSame(TypType.int_type)){//both are int
                    writeCalc(expr.e1, expr.e2, scope);
                    switch(expr.op){
                        case Operator.ADD:
                            writeAdd("%rdi","%rax");
                            break;
                        case Operator.SUB:
                            writeSub("%rdi","%rax");
                            break;
                        case Operator.MUL:
                            writeMul("%rdi","%rax");
                            break;
                        case Operator.DIV:
                            writeDiv("%rdi");
                            break;
                        case Operator.MOD:
                            writeMod("%rdi");
                            break;

                        default:
                            break;//not arithmetic
                    }
                } else if(expr.type.isSame(TypType.bool_type)){
                    switch(expr.op){
                        case Operator.EQ:
                            writeComp(expr.e1, expr.e2, "e", scope);
                            break;
                        case Operator.DIF:
                            writeComp(expr.e1, expr.e2, "ne", scope);
                            break;
                        case Operator.LESS:
                            writeComp(expr.e1, expr.e2, "l", scope);
                            break;
                        case Operator.LESSEQ:
                            writeComp(expr.e1, expr.e2, "le", scope);
                            break;
                        case Operator.GREATER:
                            writeComp(expr.e1, expr.e2, "g", scope);
                            break;
                        case Operator.GREATEREQ:
                            writeComp(expr.e1, expr.e2, "ge", scope);
                            break;
                        
                        case Operator.OR:{
                            int mark = getMark();
                            writeExpr(expr.e1, scope);
                            writeTest("%rax","%rax");
                            writeJcc("ne", ".L" + mark);
                            writeExpr(expr.e2, scope);
                            writeLabel(".L" + mark);
                            break;
                        }
                        case Operator.AND:{
                            int mark = getMark();
                            writeExpr(expr.e1, scope);
                            writeTest("%rax","%rax");
                            writeJcc("e", ".L" + mark);
                            writeExpr(expr.e2, scope);
                            writeLabel(".L" + mark);
                            break;
                        }
                        default:
                            break;
                    }
                } else {
                    writeExpr(expr.e1, scope);
                    if(expr.e1.type.isSame(TypType.int_type)){
                        writeMov("%rax","%rdi");
                        writeCall(".int_to_string");
                    }
                    writePush("%rax");
                    
                    writeExpr(expr.e2, scope);
                    if(expr.e2.type.isSame(TypType.int_type)){
                        writeMov("%rax","%rdi");
                        writeCall(".int_to_string");
                    }
                    writeMov("%rax","%rsi");
                    writePop("%rdi");
                    writeCall(".string_concat");
                }
                break;
            }
        }
        writeVerb("end expr " + loc_com(expr.beg, expr.end));
    }

    public void writeStmt(TypStatement stmt, TypScope scope, int cont, int brk) throws Exception{
        switch(stmt.s_type){
            case TypStatement.NOP: 
                break;

            case TypStatement.EVAL: 
                writeExpr(stmt.expr, scope);
                break;

            case TypStatement.DECL:
                if(stmt.expr != null)
                    writeExpr(stmt.expr, scope);
                else
                    writeXor("%rax","%rax");
                writeMov("%rax",scope.var_loc.get(stmt.ident)+"(%rbp)");
                break;

            case TypStatement.IF:{
                writeExpr(stmt.expr, scope);
                writeTest("%rax","%rax");
                int jfalse = getMark();
                int jtrue = getMark();
                int jend = getMark();
                writeJcc("ne", ".L"+jtrue);// == 1 if true
                writeJmp(".L"+jfalse);
                writeLabel(".L"+jtrue);
                writeStmt(stmt.s_true, stmt.body, cont, brk);
                writeJmp(".L"+jend);
                writeLabel(".L"+jfalse);
                writeStmt(stmt.s_false, stmt.bodyElse, cont, brk);
                writeLabel(".L"+jend);
                break;
            }
            case TypStatement.WHILE:{
                int jbeg = getMark();
                int jend = getMark();
                writeLabel(".L"+jbeg);
                writeExpr(stmt.expr, scope);
                writeTest("%rax","%rax");
                writeJcc("e",".L"+jend);
                writeStmt(stmt.s_true, stmt.body, cont, brk);
                writeJmp(".L"+jbeg);
                writeLabel(".L"+jend);
                break;
            }
            
            case TypStatement.FOR:{
                int jbeg = getMark();
                int jinc = getMark();
                int jend = getMark();
                writeExpr(stmt.forInit, scope);
                writeMov("%rax",stmt.body.var_loc.get(stmt.ident)+"(%rbp)");
                writeJmp(".L"+jbeg);

                writeLabel(".L"+jinc);
                writeExpr(stmt.forInc, stmt.body);

                writeLabel(".L"+jbeg);
                writeExpr(stmt.expr, stmt.body);

                writeTest("%rax","%rax");
                writeJcc("e",".L"+jend);
                writeStmt(stmt.s_true, stmt.body, jinc, jend);
                writeJmp(".L"+jinc);
                writeLabel(".L"+jend);
                break;
            }

            case TypStatement.FORIT:
                if(stmt.expr.type.isArray()){
                    int jbeg = getMark();
                    int jend = getMark();
                    writeExpr(stmt.expr, scope);
                    writePush("$0");
                    writePush("%rax");
                    
                    writeLabel(".L"+jbeg);
                    
                    writeMov("(%rsp)", "%rax");
                    writeMov("8(%rsp)", "%rdi");
                    writeCmp("(%rax)","%rdi");
                    writeJcc("e",".L"+jend);
    
                    writeMov("8(%rax,%rdi,8)", "%rax");
                    write("incq 8(%rsp)");
                    writeMov("%rax",stmt.body.var_loc.get(stmt.ident)+"(%rbp)");
    
                    writeStmt(stmt.s_true, stmt.body, jbeg, jend);
                    writeJmp(".L"+jbeg);
                
                    writeLabel(".L"+jend);
                    writePop("%rdi"); 
                    writePop("%rdi"); 
                } else {
                    int jbeg = getMark();
                    int jend = getMark();
                    writeExpr(stmt.expr, scope);
                    writePush("%rax");
                    writeCall(".dyn.iterator");
                    writePop("%rdi");
                    writePush("%rax");
                    
                    writeLabel(".L"+jbeg);
                    
                    writeMov("(%rsp)", "%rax");
                    writeCall(".dyn.hasNext");
                    writeTest("%rax","%rax");
                    writeJcc("e",".L"+jend);
    
                    writeMov("(%rsp)", "%rax");
                    writeCall(".dyn.next");
                    writeMov("%rax",stmt.body.var_loc.get(stmt.ident)+"(%rbp)");
    
                    writeStmt(stmt.s_true, stmt.body, jbeg, jend);
                    writeJmp(".L"+jbeg);
                
                    writeLabel(".L"+jend);
                    writePop("%rdi");
                }
                break;

            case TypStatement.SWITCH:{
                writeExpr(stmt.expr, scope);
                writePush("%rax");
                Vector<Integer> marks = new Vector<Integer>();

                for(Pair<TypExpr, Vector<TypStatement>> ca : stmt.cases){
                    int m = getMark();
                    marks.add(m);
                    writeExpr(ca.first, scope);
                    writeCmp("(%rsp)", "%rax");
                    writeJcc("e", ".L"+m);
                }
                int jdef = getMark();
                writeJmp(".L"+jdef);
                int jafter = getMark();

                for(int i = 0; i < marks.size(); i = i+1){
                    writeLabel(".L"+marks.get(i));
                    for(TypStatement cstmt : stmt.cases.get(i).second)
                        writeStmt(cstmt, scope, cont, jafter);
                }
                
                writeLabel(".L"+jdef);
                for(TypStatement cstmt : stmt.defCase)
                    writeStmt(cstmt, scope, cont, jafter);
                
                writeLabel(".L"+jafter);
                writePop("%rdi");
                break;
            }

            case TypStatement.BLOCK:
                writeCom("scope beg");
                writeScope(stmt.body, cont, brk);
                writeCom("scope end");
                break;

            case TypStatement.RET: 
                if(stmt.expr == null || stmt.expr.type.isSame(TypType.void_type)){
                    writeMov("%rbp","%rsp");
                    writePop("%rbp");
                    writeXor("%rax", "%rax");
                    writeRet();
                } else {
                    writeExpr(stmt.expr, scope);
                    writeMov("%rbp","%rsp");
                    writePop("%rbp");
                    writeRet();
                }
                break;

            case TypStatement.CONT:
                writeJmp(".L"+cont);
                break;
            
            case TypStatement.BRK:
                writeJmp(".L"+brk);
                break;
        }
        write("");
    }

    public void writeScope(TypScope scope, int cont, int brk) throws Exception{
        for(TypStatement stmt : scope.stmts)
            writeStmt(stmt, scope, cont, brk);
    }

    String fromWho(String meth, TypClass Cl){
        if(Cl.methods.get(meth) != null)
            return Cl.syn.ident;
        return fromWho(meth, env.classes.get(Cl.ext.ident));
    }

    void writeClDesc(TypClass Cl)throws Exception{
        if(cl_done.contains(Cl.syn.ident))
            return;
        cl_done.add(Cl.syn.ident);//avoid pb with cycle of Object
        writeClDesc(env.classes.get(Cl.ext.ident));
        writeLabel(".desc."+Cl.syn.ident);
        HashSet<String> cl_meth = new HashSet<String>();
        cl_meth.addAll(Cl.ext.listClProtos(env).keySet());
        cl_meth.addAll(Cl.methods.keySet());
        for(int i = 0; i < meth_nb; ++i)
            if(cl_meth.contains(meth_done.get(i)))
                write(".quad .method."+fromWho(meth_done.get(i), Cl)+"."+meth_done.get(i));
            else 
                write(".quad 0");
        for(String pid : cl_meth)
            if(meth_addr.get(pid) == null){
                write(".quad .method."+fromWho(pid, Cl)+"."+pid);
                meth_done.add(pid);
                meth_addr.put(pid, meth_nb);
                ++meth_nb;
            }
    }

    void writeClMethod(TypClass Cl)throws Exception{
        //constructor
        writeCom("class "+Cl.syn.ident);
        {
            TypConstructor c = Cl.constructor;
            writeLabel(".construct."+Cl.syn.ident);
            writePush("%rbp");
            writeMov("%rsp","%rbp");

            writePush("16(%rbp)");
            writeCall(".construct."+Cl.ext.ident);
            writePop("%rdi");
            
            writeSub("$"+(  - c.body.max_depth),"%rsp");
            writeScope(c.body, -1, -1);
            writeMov("%rbp","%rsp");
            writePop("%rbp");
            writeXor("%rax", "%rax");
            writeRet();
        }

        for(TypMethod meth : Cl.methods.values()){
            writeLabel(".method."+Cl.syn.ident+"."+meth.ident);
            writePush("%rbp");
            writeMov("%rsp","%rbp");
            writeSub("$"+(  - meth.body.max_depth),"%rsp");
            writeScope(meth.body, -1, -1);
            if(meth.retType.isSame(TypType.void_type)){
                writeMov("%rbp","%rsp");
                writePop("%rbp");
                writeXor("%rax", "%rax");
                writeRet();
            }
        }
    }

    public void writeClStaticMethod(TypClass Cl)throws Exception{
        //constructor
        for(TypMethod meth : Cl.static_methods.values()){
            writeLabel(".static.method."+Cl.syn.ident+"."+meth.ident);
            writePush("%rbp");
            writeMov("%rsp","%rbp");
            writeSub("$"+(  - meth.body.max_depth),"%rsp");
            writeScope(meth.body, -1, -1);
            if(meth.retType.isSame(TypType.void_type)){
                writeMov("%rbp","%rsp");
                writePop("%rbp");
                writeXor("%rax", "%rax");
                writeRet();
            }
        }
    }

    void writeStaticInit(TypClass cl, String id, 
            TypExpr expr, HashSet<String> done) throws Exception{
        String fullid = cl.syn.ident + "." + id;
        if(done.contains(fullid))
            return;

        for(Pair<TypClass, String> p : env.expr_dep.get(fullid))
            writeStaticInit(p.first, p.second, p.first.static_init.get(p.second), done);

        writeExpr(expr, null);
        write("movq %rax, .static.var."+fullid);
        done.add(fullid);
    }

    public void writeCode() throws Exception{
        cl_done = new HashSet<String>();
        meth_addr = new Hashtable<String, Integer>();
        meth_done = new Vector<String>();
        meth_nb = 0;
        write(".data");
        for(TypClass cl : env.classes.values())
            for(String id : cl.static_fields.keySet()){
                writeLabel(".static.var."+cl.syn.ident+"."+id);
                write(".quad 0");
            } 

        write(".text");
        write(".globl main");
        //utils
        writeCom("utils");
        {
            writeLabel(".alloc_str");
            write("pushq %rdi");
            write("addq $17, %rdi");//2 quad and \0 in addition
            write("pushq %rbp");
            write("movq %rsp, %rbp");
            write("addq $(-16), %rsp");
            write("call malloc");
            write("movq %rbp,%rsp");
            write("popq %rbp");
            write("popq %rdi");

            write("movq $.desc.String, (%rax)");//string desc
            write("movq %rdi, 8(%rax)");//string length 
            write("movb $0, 16(%rax, %rdi)");// \0
            write("ret");
        }
        write("");
        {
            writeLabel(".int_to_string");
            write("xorq %r8, %r8");
            write("testq %rdi, %rdi");
            write("sets %r8b");//r8 : bool is neg

            write("testq %r8, %r8");
            write("je .LH0");
            //only positive number used, negative ones are special case later
            write("negq %rdi");
            writeLabel(".LH0");

            write("movq %rdi, %rax");
            write("xorq %rdi, %rdi");
            write("movq $10, %r9");

            //push on stack the digits (and count them)
            writeLabel(".LH1");
            write("xorq %rdx, %rdx");
            write("divq %r9");
            write("addq $48, %rdx");//'0' == 48
            write("pushq %rdx");
            write("incq %rdi");
            write("testq %rax, %rax");
            write("jne .LH1");

            write("testq %r8, %r8");
            write("je .LH2");
            write("pushq $45");//'-' == 45
            write("incq %rdi");
            writeLabel(".LH2");

            write("call .alloc_str");//%rdi calee-save for alloc_str
            write("xorq %rdx, %rdx");

            //write the number in the allocated string
            writeLabel(".LH3");
            write("popq %r9");
            write("movb %r9b, 16(%rax,%rdx)");
            write("incq %rdx");
            write("decq %rdi");
            write("jne .LH3");
            
            write("ret");
        }
        write("");
        {
            writeLabel(".string_concat");//rdi..rsi
            
            //alloc
            write("movq %rdi, %rdx");
            write("movq 8(%rdx), %rdi");
            write("addq 8(%rsi), %rdi");
            write("pushq %rdx");
            write("pushq %rsi");
            write("call .alloc_str");
            write("popq %rsi");
            write("popq %rdx");
            
            write("xorq %r8, %r8");
            //cpy rdi (now rdx)
            write("movq 8(%rdx), %rdi");
            write("testq %rdi, %rdi");
            write("je .LH5");

            writeLabel(".LH4");
            write("movb 16(%rdx,%r8), %r10b");
            write("movb %r10b, 16(%rax,%r8)");
            write("incq %r8");
            write("decq %rdi");
            write("jne .LH4");
            writeLabel(".LH5");

            //cpy rsi
            write("xorq %r9, %r9");
            write("movq 8(%rsi), %rdi");
            write("testq %rdi, %rdi");
            write("je .LH7");

            writeLabel(".LH6");
            write("movb 16(%rsi,%r9), %r10b");
            write("movb %r10b, 16(%rax,%r8)");
            write("incq %r8");
            write("incq %r9");
            write("decq %rdi");
            write("jne .LH6");
            writeLabel(".LH7");

            write("ret");
        }
        write("");

        markCount = 0;

        write(".data"); 
        //descriptors
        writeCom("class descritors");
        for(TypClass cl : env.classes.values()){
            writeClDesc(cl);
        }
        write("");

        write(".text");
        //class methods
        writeCom("class methods");
        writeCom("default classes");
        {
            writeLabel(".construct.Object");//Object constructor does nothing
            writeRet();

            writeLabel(".construct.String");//String constructor does nothing
            writeRet();

            writeLabel(".method.String.equals");
            //arg en rsp + 8 et rsp + 16
            write("movq  8(%rsp), %rdi");
            write("movq 16(%rsp), %rsi");
            
            write("movq 8(%rdi), %r8");//cmp length
            write("cmpq 8(%rsi), %r8");
            write("je .LH8");
            write("xorq %rax, %rax");
            write("ret");

            writeLabel(".LH8");//cmp char by char, in reverse
            write("testq %r8, %r8");
            write("je .LH9");

            write("decq %r8");
            write("movb 16(%rdi,%r8), %r9b");
            write("movb 16(%rsi,%r8), %r10b");
            
            write("cmpb %r9b, %r10b");
            write("je .LH8");
            write("xorq %rax, %rax");
            write("ret");

            writeLabel(".LH9");
            write("movq $1, %rax");
            write("ret");

            writeLabel(".construct.System");//System constructor does nothing
            writeRet();
            
            writeLabel(".construct.System.out");//System constructor does nothing
            writeRet();

            writeLabel(".method.System.out.print");
            write("movq 16(%rsp), %rsi");
            write("leaq 16(%rsi), %rsi");

            write(".data");
            writeLabel("0");
            write(".string \"%s\"");
            write(".text");

            write("movq $0b, %rdi");
            write("xorq %rax, %rax");
            write("pushq %rbp");
            write("movq %rsp, %rbp");
            write("andq $(-16), %rsp");
            write("call printf");
            write("movq %rbp, %rsp");
            write("popq %rbp");
            writeRet();
            
            writeLabel(".method.System.out.println");
            write("movq 16(%rsp), %rsi");
            write("leaq 16(%rsi), %rsi");

            write(".data");
            writeLabel("0");
            write(".string \"%s\\n\"");
            write(".text");

            write("movq $0b, %rdi");
            write("xorq %rax, %rax");
            write("pushq %rbp");
            write("movq %rsp, %rbp");
            write("andq $(-16), %rsp");
            write("call printf");
            write("movq %rbp, %rsp");
            write("popq %rbp");
            writeRet();
        }
        writeCom("prgm classes");
        for(TypClass cl : env.classes.values()){
            if(!cl.syn.ident.equals("Object") && 
                    !cl.syn.ident.equals("String") &&
                    !cl.syn.ident.equals("System") &&
                    !cl.syn.ident.equals("System.out"))
                writeClMethod(cl);
        }
        write("");
        writeCom("Static methods");
        writeCom("default classes");
        {
            writeLabel(".static.method.System.exit");
            write("movq 8(%rsp), %rax");
            write("movq .static.var.System.exit_point, %rsp");
            writeRet();
        }
        writeCom("prgm classes");
        for(TypClass cl : env.classes.values()){
            if(!cl.syn.ident.equals("Object") && 
                    !cl.syn.ident.equals("String") &&
                    !cl.syn.ident.equals("System"))
                writeClStaticMethod(cl);
        }
        write("");
        
        writeCom("method callers");
        for(int i = 0; i < meth_nb; ++i){
            writeLabel(".dyn."+meth_done.get(i));
            write("movq (%rax),%rax");//method call always also take this in %rax
            write("jmp *"+ i*8 +"(%rax)");
        }
        for(TypInterface intf : env.interfaces.values())
            for(String pid : intf.protos.keySet())
                if(meth_addr.get(pid) == null){
                    writeLabel(".dyn."+pid);
                    write("movq (%rax),%rax");//method call always also take this in %rax
                    write("jmp *"+ meth_nb*8 +"(%rax)");
                    
                    meth_addr.put(pid, meth_nb);
                    ++meth_nb;
                    meth_done.add(pid);
                }
        write("");
        writeCom("builders");
        for(TypClass cl : env.classes.values()){
            writeLabel(".build."+cl.syn.ident);
            write("movq $"+(cl.size+8)+" ,%rdi");
            
            write("pushq %rbp");
            write("movq %rsp, %rbp");
            write("andq $(-16), %rsp");
            write("call malloc");
            write("movq %rbp, %rsp");
            write("popq %rbp");
            
            write("movq $.desc."+cl.syn.ident+", (%rax)");
            for(int i = 0; i < cl.size/8; ++i)
                write("movq $0,"+ (8*i+8)+"(%rax)");
            write("popq %rdi");
            write("pushq %rax");
            write("pushq %rdi");
            write("jmp .construct."+cl.syn.ident);
        }
        write("");

        //main
        
        HashSet<String> static_done = new HashSet<String>();
        
        writeCom("main");
        writeLabel("main");
        write("pushq %rbp");
        write("movq %rsp, %rbp");
        write("");
        writeCom("arguments reading");
        {
            write("movq %rdi, %r12");
            write("movq %rsi, %r13");
            write("imulq $8, %rdi");
            write("addq $8, %rdi");
            write("call malloc");
            write("movq %r12, (%rax)");
            write("pushq %rax");
            write("movq %rax, %r14");
            write("xorq %r15, %r15");
            
            writeLabel(".LM4");//main loop
            write("movq (%r13, %r15, 8), %rbx");
            write("xorq %rdi, %rdi");
            write("movb (%rbx), %sil");

            writeLabel(".LM1");//count strlen
            write("testb %sil, %sil");
            write("jz .LM2");

            write("incq %rdi");
            write("movb (%rbx, %rdi), %sil");
            write("jmp .LM1");

            writeLabel(".LM2");//alloc
            write("call .alloc_str");
            write("movq %rax, 8(%r14, %r15, 8)");
            write("xorq %r8, %r8");

            writeLabel(".LM3");//cpy
            write("movb (%rbx, %r8), %sil");
            write("testb %sil, %sil");
            write("jz .LM5");

            write("movb %sil, 16(%rax, %r8)");
            write("incq %r8");
            write("jmp .LM3");

            writeLabel(".LM5");
            write("incq %r15");
            write("cmpq %r15, %r12");
            write("jne .LM4");
            write("");
        }
        writeCom("static var initialisation");

        write("leaq 16(%rsp), %rax");
        write("movq %rax, .static.var.System.exit_point");
        write("call .build.System.out");
        write("popq %rax");
        write("movq %rax, .static.var.System.out");
        for(TypClass cl : env.classes.values())
            if(!cl.syn.ident.equals("System"))
                for(String id : cl.static_fields.keySet())
                    writeStaticInit(cl, id, cl.static_init.get(id), static_done);
        write("call .static.method.Main.main");
        write("movq %rbp, %rsp");
        write("popq %rbp");
        write("xorq %rax, %rax");
        writeRet();
        write("");
    }
}
