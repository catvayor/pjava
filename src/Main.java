import java.lang.System;
import java.util.*;
import java.io.*;
import syn.*;
import typ.*;
import global.*;
import analyser.*;

public class Main{
    public static void main(String[] args){
        String infile = "";
        int level = 2;//0 for parse, 1 type, 2 code_gen
        boolean verbose = false;

        for(String arg : args){
            if(arg.equals("--verbose"))
                verbose = true;
            else if(arg.equals("--parse-only"))
                level = 0;
            else if(arg.equals("--type-only"))
                level = 1;
            else
                infile = arg;
        }
        
        if(infile.equals("")){
            System.out.println("no input file !");
            System.exit(2);
        }

        if(!infile.substring(infile.length() - 5, infile.length()).equals(".java")){
            System.out.println("expected .java file");
            System.exit(2);
        }

        String outfile = infile.substring(0, infile.length() - 5) + ".s";

        OrrorManager orr_man = new OrrorManager(infile);

        try{
            File input = new File(infile);
            if(!input.canRead()){
                System.out.println("permission to read " + infile + "requiered");
                System.exit(2);
            }

            Lexer lex = new Lexer(new FileReader(input), orr_man);
            parser p = new parser(lex, orr_man);
            
            p.parse();
            orr_man.flush_orrors();
            
            if(level == 0)
                return;

            TypEnvironment env = new TypEnvironment(orr_man);
            
            for(SynClassIntf ci : p.prog){
                env.add_classintf(ci);
            }
            orr_man.flush_orrors();

            env.build_params();
            orr_man.flush_orrors();
            
            env.check_inheritance();
            orr_man.flush_orrors();

            env.build_over_types();
            env.checkValidity();
            env.buildProtos();
            env.buildBodies();
            env.checkProtosConflict();
            orr_man.flush_orrors();
            
            env.check_stinit();
            env.check_main();
            orr_man.flush_orrors();
            
            if(level == 1)
                return;

            FileWriter output = new FileWriter(outfile);
           
            env.calcFieldsIdx();

            Assemb assemb = new Assemb(output, env, verbose);
            assemb.writeCode();

            output.close();
        }
        catch (Exception e){
            System.out.println("crash " + e.toString());
            System.exit(2);
        }
    }
}
