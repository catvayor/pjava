package analyser;
import java.io.InputStreamReader;
import java.util.Hashtable;
import global.*;

public class Lexer {
    static final int DEF    = 0;
    static final int LINCOM = 1;
    static final int BLKCOM = 2;
    static final int STR    = 3;
    static final int ID     = 4;
    static final int INT    = 5;

    InputStreamReader in_stream;
    OrrorManager orr_man;
    Hashtable<String, Integer> keywords;
    Hashtable<String, Integer> tokens;
    String buf;
    Location beg;
    
    Location curr(){
        return new Location(line, col);
    }
    
    int col;
    int line;

    int last_r;

    int status;

    public Lexer(InputStreamReader in, OrrorManager man){
        col = 0;
        line = 1;
        in_stream = in;
        status = DEF;
        last_r = -2;
        orr_man = man;

        keywords = new Hashtable<String, Integer>();

        keywords.put("boolean"   , sym.BOOLEAN);
        keywords.put("class"     , sym.CLASS);
        keywords.put("else"      , sym.ELSE);
        keywords.put("extends"   , sym.EXTENDS);
        keywords.put("false"     , sym.FALSE);
        keywords.put("if"        , sym.IF);
        keywords.put("implements", sym.IMPLEMENTS);
        keywords.put("int"       , sym.INT);
        keywords.put("interface" , sym.INTERFACE);
        keywords.put("new"       , sym.NEW);
        keywords.put("null"      , sym.NULL);
        keywords.put("public"    , sym.PUBLIC);
        keywords.put("return"    , sym.RETURN);
        keywords.put("static"    , sym.STATIC);
        keywords.put("this"      , sym.THIS);
        keywords.put("true"      , sym.TRUE);
        keywords.put("void"      , sym.VOID);
        keywords.put("while"     , sym.WHILE);
        keywords.put("break"     , sym.BREAK);
        keywords.put("continue"  , sym.CONTINUE);
        keywords.put("for"       , sym.FOR);
        keywords.put("switch"    , sym.SWITCH);
        keywords.put("case"      , sym.CASE);
        keywords.put("final"     , sym.FINAL);
        keywords.put("default"   , sym.DEFAULT);
        
        keywords.put("import"   , sym.IMPORT);
        keywords.put("package"   , sym.PACKAGE);
        
        tokens = new Hashtable<String, Integer>();
        
        tokens.put("=" , sym.ASIGN);
        tokens.put("||", sym.OR);
        tokens.put("&&", sym.AND);
        tokens.put("==", sym.EQUAL);
        tokens.put("!=", sym.DIFFERENT);
        tokens.put(">" , sym.GREATER);
        tokens.put(">=", sym.GREATEREQ);
        tokens.put("<" , sym.LESS);
        tokens.put("<=", sym.LESSEQ);
        tokens.put("+" , sym.PLUS);
        tokens.put("-" , sym.MINUS);
        tokens.put("*" , sym.TIMES);
        tokens.put("/" , sym.SLASH);
        tokens.put("%" , sym.MODULO);
        tokens.put("!" , sym.NOT);
        tokens.put("." , sym.DOT);
        tokens.put("(" , sym.LBRACE);
        tokens.put(")" , sym.RBRACE);
        tokens.put("{" , sym.LCBRACKET);
        tokens.put("}" , sym.RCBRACKET);
        tokens.put("[" , sym.LBRACKET);
        tokens.put("]" , sym.RBRACKET);
        tokens.put(";" , sym.SEMICOLON);
        tokens.put("," , sym.COMMA);
        tokens.put(":" , sym.COLON);
        tokens.put("&" , sym.AMPERSAND);
    }

    void orror(String msg){
        orr_man.fatal_orror(new Orror(Orror.LEXICAL, beg, curr(), msg));
    }

    Symbol symbol0(int code){
        return new Symbol(code, beg, curr(), 0, null);
    }
    
    Symbol symbolStr(int code, String data){
        return new Symbol(code, beg, curr(), 0, data);
    }
    
    Symbol symbolInt(int code, int data){
        return new Symbol(code, beg, curr(), data, null);
    }

    Symbol ident(String id){
        Integer keyID = keywords.get(id);
        if(keyID != null){
            return symbol0(keyID);
        }
        else
            return symbolStr(sym.IDENT, id);
    }

    Symbol tok(String token){
        Integer tokID = tokens.get(token);
        if(tokID != null)
            return symbol0(tokID);
        orror("unrecognized token '"+token+"'");
        return null;
    }

    int read() throws java.io.IOException {
        if(last_r == -2)
            return force_read(); 
        else
            return last_r;
    }
    int force_read() throws java.io.IOException {
        col = col+1;
        return last_r = in_stream.read();
    }

    Symbol decode_buf(){
        //System.out.println("decoding "+buf+" status : "+status);
        Symbol ret = null;
        switch(status){
            case STR:
                ret = symbolStr(sym.STR_LIT, buf);
                status = DEF;
                break;
            case INT:
                ret = symbolInt(sym.INT_LIT, Integer.parseInt(buf));
                status = DEF;
                break;
            case ID:
                ret = ident(buf);
                status = DEF;
                break;
            case DEF:
                ret = tok(buf);
                break;
            default:
                break;
        }
        buf = "";
        return ret;
    }

    void newline(){
        col = 1;
        line = line+1;
    }
    boolean checkNewline(char c)throws java.io.IOException{
        if(c == '\r'){
            newline();
            int nc = force_read();
            if(nc == -1)
                return true;
            if((char)nc == '\n'){
                col = 0;
                last_r = -2;
            }
            return true;
        }
        if(c == '\n'){
            newline();
            last_r = -2;
            return true;
        }
        return false;
    }

    boolean statusUpdate(char c)throws java.io.IOException{
        switch(status){
            case LINCOM:
                if(checkNewline(c))
                    status = DEF;
                last_r = -2;
                break;
            case BLKCOM:
                if(checkNewline(c))
                    return false;
                while(c == '*'){
                    int nc = force_read();
                    if(nc == -1)
                        orror("unclosed comment");
                    c = (char)nc;
                    last_r = -2;
                    if(c == '/'){
                        status = DEF;
                        return false;
                    }
                }
                last_r = -2;
                break;
            case STR:
                if(checkNewline(c))
                    orror("unclosed string literal");
                if(c == '\\'){
                    buf = buf+c;
                    int nc = force_read();
                    if(nc == -1)
                        orror("unclosed string literal");
                    c = (char)nc;
                    last_r = -2;
                    if(c == '\"'){
                        buf = buf+c;
                        return false;
                    }
                    if(c == '\\'){
                        buf = buf+c;
                        return false;
                    }
                    if(c == 'n'){
                        buf = buf+c;
                        return false;
                    }
                    beg = curr();
                    orror("illegal escape caracter '\\"+c+"'");
                }
                if(c == '\"'){
                    last_r = -2;
                    return true;
                }
                buf = buf+c;
                last_r = -2;
                break;
            case INT:
                if(Character.isDigit(c)){
                    last_r = -2;
                    buf = buf+c;
                    break;
                }
                else
                    return true;
            case ID:
                if(Character.isJavaIdentifierPart(c)){
                    last_r = -2;
                    buf = buf+c;
                    break;
                }
                else
                    return true;
            case DEF:
                if(checkNewline(c))
                    return false;
                beg = curr();
                if(Character.isWhitespace(c)){
                    last_r = -2;
                    return false;
                }
                if(Character.isJavaIdentifierStart(c)){
                    status = ID;
                    break;
                }
                if(Character.isDigit(c)){
                   status = INT;
                   break;
                }
                if(c == '\"'){
                    last_r = -2;
                    status = STR;
                    break;
                }
                
                buf = ""+c;

                switch(c){
                    case '/':{
                        int nc = force_read();
                        if(nc == -1){ return true; }
                        c = (char)nc;
                        if(c == '/'){
                            last_r = -2;
                            status = LINCOM;
                            buf = "";
                            return false;
                        }
                        else if(c == '*'){
                            last_r = -2;
                            status = BLKCOM;
                            buf = "";
                            return false;
                        }
                        else return true;
                    }
                    case '|':{
                        int nc = force_read();
                        if(nc == -1 || (char)nc != '|')
                            return true;
                        last_r = -2;
                        buf = "||";
                        return true;
                    }
                    case '&':{
                        int nc = force_read();
                        if(nc == -1){ return true; }
                        c = (char)nc;
                        if(c == '&'){
                            buf = buf+c;
                            last_r = -2;
                        }
                        return true;
                    }
                    case '=':
                    case '!':
                    case '>':
                    case '<':{
                        int nc = force_read();
                        if(nc == -1){ return true; }
                        c = (char)nc;
                        if(c == '='){
                            buf = buf+c;
                            last_r = -2;
                        }
                        return true;
                    }
                    case '+':
                    case '*':
                    case '%':
                    case '-':
                    case '.':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                    case ':':
                    case ';':
                    case ',':
                        last_r = -2;
                        return true;
                    default:
                        orror("illegal caracter '"+c+"'");
                }
                return false;
        }
        return false;
    }

    public Symbol next_token() throws java.io.IOException{
        boolean dummy = true;
        buf = "";
        while(dummy){
            int c = read();
            if(c == -1){
                if(!buf.equals(""))
                    return decode_buf();
                else switch(status){
                    case STR:
                        orror("unclosed string literal");
                    case BLKCOM:
                        orror("unclosed comment");
                    case LINCOM:
                    case DEF:
                        return symbol0(sym.EOF);
                }
            }
            else if(statusUpdate((char)c))
                return decode_buf();
        }
        return null;//never happen
    }
}

