package analyser;
import global.*;

class Symbol{
    int code;

    Location beg;
    Location end;

    int i_data;
    String str_data;

    Symbol(int c, Location b, Location e, int i, String str){
        beg = b;
        end = e;
        i_data = i;
        str_data = str;
        code = c;
    }
}
