package analyser;
import syn.*;
import global.*;
import java.util.Vector;

public class parser{
    OrrorManager orr_man;
    Lexer lex;

    public Vector<SynClassIntf> prog;

    Symbol last_tok;
    Location prec_tok_end;
    
    public parser(Lexer lexer, OrrorManager man){
        lex = lexer;
        orr_man = man;
        last_tok = new Symbol(sym.error, null, null, 0, null);
        prog = new Vector<SynClassIntf>();
    }
    
    String strForSym(int symbol){
        switch(symbol){
        case sym.ASIGN:      return "=";
        case sym.OR:         return "||";
        case sym.AND:        return "&&";
        case sym.EQUAL:      return "==";
        case sym.DIFFERENT:  return "!=";
        case sym.GREATER:    return ">";
        case sym.GREATEREQ:  return ">=";
        case sym.LESS:       return "<";
        case sym.LESSEQ:     return "<=";
        case sym.PLUS:       return "+";
        case sym.MINUS:      return "-";
        case sym.TIMES:      return "*";
        case sym.SLASH:      return "/";
        case sym.MODULO:     return "%";
        case sym.NOT:        return "!";
        case sym.DOT:        return ".";

        case sym.LBRACE:     return "(";
        case sym.RBRACE:     return ")";
        case sym.LCBRACKET:  return "{";
        case sym.RCBRACKET:  return "}";
        case sym.LBRACKET:   return "[";
        case sym.RBRACKET:   return "]";

        case sym.BOOLEAN:    return "'boolean'";
        case sym.CLASS:      return "'class'";
        case sym.ELSE:       return "'else'";
        case sym.EXTENDS:    return "'extends'";
        case sym.FALSE:      return "'false'";
        case sym.IF:         return "'if'";
        case sym.IMPLEMENTS: return "'implements'";
        case sym.INT:        return "'int'";
        case sym.INTERFACE:  return "'interface'";
        case sym.NEW:        return "'new'";
        case sym.NULL:       return "'null'";
        case sym.PUBLIC:     return "'public'";
        case sym.RETURN:     return "'return'";
        case sym.STATIC:     return "'static'";
        case sym.THIS:       return "'this'";
        case sym.TRUE:       return "'true'";
        case sym.VOID:       return "'void'";
        case sym.WHILE:      return "'while'";

        case sym.SEMICOLON:  return ";";
        case sym.COMMA:      return ",";
        case sym.AMPERSAND:  return "&";

        case sym.IDENT:      return "identifier";
        case sym.INT_LIT:    return "integer literal";
        case sym.STR_LIT:    return "String literal";

        case sym.EOF:        return "end of file";

        case sym.BREAK:      return "'break'";
        case sym.CONTINUE:   return "'continue'";
        case sym.FOR:        return "'for'";
        case sym.SWITCH:     return "'switch'";
        case sym.CASE:       return "'case'";
        case sym.COLON:      return ":";
        case sym.PACKAGE:    return "'package'";
        case sym.IMPORT:     return "'import'";
        case sym.FINAL:      return "'final'";
        case sym.DEFAULT:    return "'default'";

        case sym.error:      return "<error>";

        default:             return "SYMBOL" + symbol;
        }
    }

    public void parse(){
        prog = new Vector<SynClassIntf>();
        next_tok();
        while(last_tok.code != sym.EOF)
            prog.add(parseClassIntf());
    }

    void orror(Location beg, Location end, String msg){
        orr_man.fatal_orror(new Orror(Orror.SYNTAX, beg, end, msg));
    }

    void next_tok(){
        try{
            prec_tok_end = last_tok.end;
            last_tok = lex.next_token();
        } catch(Exception e){
            System.out.println("crash "+e);
            System.exit(2);
        }
    }

    void assertSymb(int code){
        if(last_tok.code == code)
            next_tok();
        else 
            orror(last_tok.beg, last_tok.end, "expected " + strForSym(code) +
                                        ", got " + strForSym(last_tok.code));
    }

    Vector<SynParamtype> parseParamtypes(){
        Vector<SynParamtype> ret = null;
        if(last_tok.code == sym.LESS){
            ret = new Vector<SynParamtype>();
            last_tok.code = sym.COMMA; //just for entering in loop
            while(last_tok.code == sym.COMMA){
                next_tok();
                Location beg = last_tok.beg;

                String ident = last_tok.str_data;
                assertSymb(sym.IDENT);
                
                Vector<SynNtype> exts = null;
                if(last_tok.code == sym.EXTENDS){
                    exts = new Vector<SynNtype>();
                    last_tok.code = sym.AMPERSAND;
                    while(last_tok.code == sym.AMPERSAND){
                        next_tok();
                        exts.add(parseNtype());
                    }
                }
                ret.add(new SynParamtype(ident, exts, beg, prec_tok_end));
            }
            assertSymb(sym.GREATER);
        }
        return ret;
    }

    SynClassIntf parseClassIntf(){
        Location beg = last_tok.beg;

        if(last_tok.code == sym.CLASS){
            next_tok();
            String ident = last_tok.str_data;
            assertSymb(sym.IDENT);
            Vector<SynParamtype> params = parseParamtypes();
            
            SynNtype ext = null;
            if(last_tok.code == sym.EXTENDS){
                next_tok();
                ext = parseNtype();
            }

            Vector<SynNtype> impls = null;
            if(last_tok.code == sym.IMPLEMENTS){
                impls = new Vector<SynNtype>();
                last_tok.code = sym.COMMA;
                while(last_tok.code == sym.COMMA){
                    next_tok();
                    impls.add(parseNtype());
                }
            }
            Location end = prec_tok_end;

            Vector<SynDecl> decls = new Vector<SynDecl>();
            assertSymb(sym.LCBRACKET);
            while(last_tok.code != sym.RCBRACKET)
                decls.add(parseDecl());
            next_tok();

            return new SynClassIntf(ident, params, ext, impls, decls, beg, end);
        } else if(last_tok.code == sym.INTERFACE){
            next_tok();

            String ident = last_tok.str_data;
            assertSymb(sym.IDENT);

            Vector<SynParamtype> params = parseParamtypes();
            
            Vector<SynNtype> exts = null;
            if(last_tok.code == sym.EXTENDS){
                exts = new Vector<SynNtype>();
                last_tok.code = sym.COMMA;
                while(last_tok.code == sym.COMMA){
                    next_tok();
                    exts.add(parseNtype());
                }
            }
            Location end = prec_tok_end;

            Vector<SynProto> protos = new Vector<SynProto>();
            assertSymb(sym.LCBRACKET);
            while(last_tok.code != sym.RCBRACKET)
                protos.add(parseProto());
            next_tok();

            return new SynClassIntf(ident, params, exts, protos, beg, end);
        } else{
            orror(last_tok.beg, last_tok.end, "expected " + strForSym(sym.CLASS) +
                                        " or " + strForSym(sym.INTERFACE) +
                                        ", got " + strForSym(last_tok.code));
            return null;
        }
    }

    void parseNtypeParams(SynNtype nt){
        nt.gen_param = new Vector<SynNtype>();
        last_tok.code = sym.COMMA;
        while(last_tok.code == sym.COMMA){
            next_tok();
            nt.gen_param.add(parseNtype());
        }
        assertSymb(sym.GREATER);
        nt.end = prec_tok_end;
    }

    SynNtype parseNtype(){
        SynNtype nt = new SynNtype(last_tok.str_data, null, last_tok.beg, last_tok.end);
        assertSymb(sym.IDENT);
        if(last_tok.code == sym.LESS){
            parseNtypeParams(nt);
        }
        return nt;
    }

    SynType parseArr(SynType ty){
        if(last_tok.code == sym.LBRACKET){
            next_tok();
            SynType t = new SynType(ty, ty.beg, last_tok.end);
            assertSymb(sym.RBRACKET);
            return parseArr(t);
        }
        return ty;
    }

    SynType parseType(boolean canVoid){
        SynType ret;
        if(last_tok.code == sym.BOOLEAN){
            ret = new SynType(SynType.BOOL, last_tok.beg, last_tok.end);
            next_tok();
        }
        else if(last_tok.code == sym.INT){
            ret = new SynType(SynType.INT, last_tok.beg, last_tok.end);
            next_tok();
        }
        else if(last_tok.code == sym.VOID){
            ret = new SynType(SynType.VOID, last_tok.beg, last_tok.end);
            if(!canVoid)
                orror(last_tok.beg, last_tok.end, "unexpected " + strForSym(sym.VOID) + " token");
            next_tok();
        } else {
            SynNtype nt = parseNtype();
            ret = new SynType(nt, nt.beg, nt.end);
        }
        return parseArr(ret);
    }

    SynParameter parseArg(){
        SynType ty = parseType(false);
        String ident = last_tok.str_data;
        assertSymb(sym.IDENT);
        return new SynParameter(ty, ident, ty.beg, prec_tok_end);
    }

    Vector<SynParameter> parseFuncArgs(){
        assertSymb(sym.LBRACE);
        Vector<SynParameter> args = new Vector<SynParameter>();
        if(last_tok.code != sym.RBRACE){
            args.add(parseArg());
            while(last_tok.code == sym.COMMA){
                next_tok();
                args.add(parseArg());
            }
        }
        assertSymb(sym.RBRACE);
        return args;
    }

    SynProto parseProto(){
        SynType ty = parseType(true);
        String ident = last_tok.str_data;
        assertSymb(sym.IDENT);
        Vector<SynParameter> args = parseFuncArgs();
        Location end = prec_tok_end;
        assertSymb(sym.SEMICOLON);
        return new SynProto(false, false, ty, ident, args, ty.beg, end);
    }

    Vector<SynStatement> parseStmtBlock(){
        assertSymb(sym.LCBRACKET);
        Vector<SynStatement> ret = new Vector<SynStatement>();
        while(last_tok.code != sym.RCBRACKET){
            ret.add(parseStmt());
        }
        next_tok();
        return ret;
    }

    SynDecl parseDecl(){
        Location beg = last_tok.beg;
        boolean public_ = false;
        if(last_tok.code == sym.PUBLIC){
            public_ = true;
            next_tok();
        }

        if(last_tok.code == sym.STATIC){
            next_tok();
            
            if(last_tok.code == sym.FINAL){
                // (public)? static final <type> ident = <expr>;
                //                       |
                next_tok();
                SynType ty = parseType(false);
                String ident = last_tok.str_data;
                assertSymb(sym.IDENT);
                assertSymb(sym.ASIGN);
                SynExpr init = parseExpr();
                assertSymb(sym.SEMICOLON);
                //static final var
                return new SynDecl(public_, true, true, ty, ident, init, ty.beg, prec_tok_end);
            }
    
            //      (public)? static <type> ident (= <expr>)? ;
            // or   ----------------|------------ ( (<type> ident)*_, ) <stmt_block>
            SynType ty = parseType(true);
            String ident = last_tok.str_data;
            assertSymb(sym.IDENT);

            if(last_tok.code == sym.LBRACE){
                Vector<SynParameter> args = parseFuncArgs();
                SynProto proto = new SynProto(public_, true, ty, ident, args, beg, prec_tok_end);
                
                Vector<SynStatement> body = parseStmtBlock();
                SynMethod meth = new SynMethod(proto, body);
                //static method
                return new SynDecl(meth);
            }

            //static var
            SynDecl ret = new SynDecl(public_, true, false, ty, ident, 
                    new SynExpr(), ty.beg, prec_tok_end);

            if(last_tok.code == sym.ASIGN){
                next_tok();
                ret.init = parseExpr();
            }
            assertSymb(sym.SEMICOLON);
            return ret;
        }
        
        if(last_tok.code != sym.IDENT){
            //      (public)? <type> ident (= <expr>)? ;
            // or   ---------|------------ ( (<type> ident)*_, ) <stmt_block>
            SynType ty = parseType(true);
            String ident = last_tok.str_data;
            assertSymb(sym.IDENT);

            if(last_tok.code == sym.LBRACE){
                //method
                Vector<SynParameter> args = parseFuncArgs();
                SynProto proto = new SynProto(public_, false, ty, ident, args, beg, prec_tok_end);
                
                Vector<SynStatement> body = parseStmtBlock();
                SynMethod meth = new SynMethod(proto, body);
                return new SynDecl(meth);
            }
            //class var
            assertSymb(sym.SEMICOLON);
            return new SynDecl(public_, false, false, ty, ident, 
                    new SynExpr(), ty.beg, prec_tok_end);
        }

        String ident = last_tok.str_data;
        Location ntbeg = last_tok.beg;

        next_tok();
        if(last_tok.code == sym.LBRACE){
            //constructor
            Vector<SynParameter> args = parseFuncArgs();
            Location end = prec_tok_end;
            Vector<SynStatement> body = parseStmtBlock();
            SynConstructor cons = new SynConstructor(ident, args, body, beg, end);
            return new SynDecl(public_, cons);
        }
        
        SynNtype nt = new SynNtype(ident, null, ntbeg, prec_tok_end);

        if(last_tok.code == sym.LESS)
            parseNtypeParams(nt);
            
        SynType ty = new SynType(nt, nt.beg, nt.end);
        ty = parseArr(ty);

        ident = last_tok.str_data;
        assertSymb(sym.IDENT);
        if(last_tok.code == sym.LBRACE){
            //method
            Vector<SynParameter> args = parseFuncArgs();
            SynProto proto = new SynProto(public_, false, ty, ident, args, beg, prec_tok_end);
                
            Vector<SynStatement> body = parseStmtBlock();
            SynMethod meth = new SynMethod(proto, body);
            return new SynDecl(meth);
        }
        //class var
        assertSymb(sym.SEMICOLON);
        return new SynDecl(public_, false, false, ty, ident, null, ty.beg, prec_tok_end);
    }

    SynStatement parseStmt(){
        SynStatement ret = null;
        switch(last_tok.code){
            case sym.SEMICOLON:
                ret = new SynStatement();
                next_tok();
                break;
            
            case sym.IDENT:{
                Location beg = last_tok.beg;
                String ident = last_tok.str_data;
                next_tok();
                if(last_tok.code == sym.LESS){
                    //<type> ident (= <expr>)? ;
                    SynNtype nt = new SynNtype(ident, null, beg, prec_tok_end);
                    parseNtypeParams(nt);
                    SynType ty = new SynType(nt, nt.beg, nt.end);
                    ty = parseArr(ty);

                    ident = last_tok.str_data;
                    ret = new SynStatement(ty, ident, null, beg, last_tok.end);
                    assertSymb(sym.IDENT);
                    
                    if(last_tok.code == sym.ASIGN){
                        next_tok();
                        ret.e = parseExpr();
                    }
                    
                    assertSymb(sym.SEMICOLON);
                    ret.end = prec_tok_end;
                    break;
                } else if(last_tok.code == sym.LBRACKET){
                    //<type> ident (= <expr>)? ;
                    SynNtype nt = new SynNtype(ident, null, beg, prec_tok_end);
                    SynType ty = new SynType(nt, nt.beg, nt.end);
                    ty = parseArr(ty);
                    
                    ident = last_tok.str_data;
                    ret = new SynStatement(ty, ident, null, beg, last_tok.end);
                    assertSymb(sym.IDENT);
                    
                    if(last_tok.code == sym.ASIGN){
                        next_tok();
                        ret.e = parseExpr();
                    }
                    
                    assertSymb(sym.SEMICOLON);
                    ret.end = prec_tok_end;
                    break;
                } else if(last_tok.code == sym.IDENT){
                    //<type> ident (= <expr>)? ;
                    SynNtype nt = new SynNtype(ident, null, beg, prec_tok_end);
                    SynType ty = new SynType(nt, nt.beg, nt.end);
                    
                    ident = last_tok.str_data;
                    ret = new SynStatement(ty, ident, null, beg, last_tok.end);
                    next_tok();
                    
                    if(last_tok.code == sym.ASIGN){
                        next_tok();
                        ret.e = parseExpr();
                    }
                    
                    assertSymb(sym.SEMICOLON);
                    ret.end = prec_tok_end;
                    break;
                } else{
                    // <expr>;
                    SynExpr e = new SynExpr(null, ident, last_tok.beg, last_tok.end);
                    parseCallOpt(e);
                    e = parseOp1(e);
                    //no op 2
                    e = parseOp3(e);
                    e = parseOp4(e);
                    e = parseOp5(e);
                    e = parseOp6(e);
                    e = parseOp7(e);
                    e = parseOp8(e);
                    e = parseOp9(e);
                    ret = new SynStatement(false, e, e.beg, last_tok.end);
                    assertSymb(sym.SEMICOLON);
                    break;
                }
            }

            case sym.INT:
            case sym.BOOLEAN:{
                SynType ty = parseType(false);
                
                String ident = last_tok.str_data;
                ret = new SynStatement(ty, ident, null, ty.beg, null);
                assertSymb(sym.IDENT);
                
                if(last_tok.code == sym.ASIGN){
                    next_tok();
                    ret.e = parseExpr();
                }
                assertSymb(sym.SEMICOLON);
                ret.end = prec_tok_end;
                break;
            }

            case sym.IF:{
                next_tok();
                assertSymb(sym.LBRACE);
                SynExpr cond = parseExpr();
                assertSymb(sym.RBRACE);

                SynStatement stmt_true = parseStmt();
                ret = new SynStatement(cond, stmt_true, null);
                if(last_tok.code == sym.ELSE){
                    next_tok();
                    ret.belse = parseStmt();
                    break;
                } else
                    break;
            }

            case sym.WHILE:{
                next_tok();
                assertSymb(sym.LBRACE);
                SynExpr cond = parseExpr();
                assertSymb(sym.RBRACE);
                SynStatement body = parseStmt();
                ret = new SynStatement(cond, body);
                break;
            }

            case sym.FOR:{
                next_tok();
                assertSymb(sym.LBRACE);
                SynType ty = parseType(false);
                String ident = last_tok.str_data;
                assertSymb(sym.IDENT);
                if(last_tok.code == sym.COLON){
                    next_tok();
                    SynExpr container = parseExpr();
                    assertSymb(sym.RBRACE);
                    SynStatement body = parseStmt();
                    ret = new SynStatement(ty, ident, container, body);
                    break;
                } else if(last_tok.code == sym.ASIGN){
                    next_tok();
                    SynExpr init = parseExpr();
                    assertSymb(sym.SEMICOLON);
                    SynExpr cond = parseExpr();
                    assertSymb(sym.SEMICOLON);
                    SynExpr incr = parseExpr();
                    assertSymb(sym.RBRACE);
                    SynStatement body = parseStmt();
                    ret = new SynStatement(ty, ident, init, cond, incr, body);
                    break;
                } else
                    orror(last_tok.beg, last_tok.end, "expected " + strForSym(sym.COLON) + 
                                                " or " + strForSym(sym.ASIGN) + 
                                                ", got " + strForSym(last_tok.code));
            }

            case sym.CONTINUE:
                ret = new SynStatement(SynStatement.CONTINUE, last_tok.beg, last_tok.end);
                next_tok();
                break;

            case sym.BREAK:
                ret = new SynStatement(SynStatement.BREAK, last_tok.beg, last_tok.end);
                next_tok();
                break;

            case sym.SWITCH:{
                next_tok();
                assertSymb(sym.LBRACE);
                SynExpr test = parseExpr();
                assertSymb(sym.RBRACE);
                assertSymb(sym.LCBRACKET);
                Vector<Pair<SynExpr,Vector<SynStatement>>> cases = 
                    new Vector<Pair<SynExpr,Vector<SynStatement> > >();
                while(last_tok.code == sym.CASE){
                    next_tok();
                    SynExpr cval = parseExpr();
                    assertSymb(sym.COLON);
                    Vector<SynStatement> cstmts = new Vector<SynStatement>();
                    while(last_tok.code != sym.CASE && last_tok.code != sym.DEFAULT)
                        cstmts.add(parseStmt());
                    cases.add(new Pair<SynExpr,Vector<SynStatement>>(cval, cstmts));
                }
                next_tok();//always DEFAULT
                assertSymb(sym.COLON);
                Vector<SynStatement> dstmts = new Vector<SynStatement>();
                while(last_tok.code != sym.RCBRACKET)
                    dstmts.add(parseStmt());
                next_tok();
                ret = new SynStatement(test, cases, dstmts);
                break;
            }

            case sym.LCBRACKET:
                ret = new SynStatement(parseStmtBlock());
                break;

            case sym.RETURN:
                ret = new SynStatement(true, null, last_tok.beg, last_tok.end);
                next_tok();
                if(last_tok.code == sym.SEMICOLON){
                    ret.end = last_tok.end;
                    next_tok();
                    break;
                } else {
                    ret.e = parseExpr();
                    ret.end = last_tok.end;
                    assertSymb(sym.SEMICOLON);
                    break;
                }
            
            default:
                SynExpr e = parseExpr();
                assertSymb(sym.SEMICOLON);
                ret = new SynStatement(false, e, e.beg, prec_tok_end);
                break;
        }
        return ret;
    }

    Vector<SynExpr> parseCall(){
        Vector<SynExpr> args = new Vector<SynExpr>();
        assertSymb(sym.LBRACE);
        if(last_tok.code != sym.RBRACE){
            args.add(parseExpr());
            while(last_tok.code == sym.COMMA){
                next_tok();
                args.add(parseExpr());
            }
        }
        assertSymb(sym.RBRACE);
        return args;
    }

    void parseCallOpt(SynExpr e){//e is VAR
        if(last_tok.code == sym.LBRACE){
            Vector<SynExpr> args = parseCall();
            e.type = SynExpr.CALL;
            e.args = args;
            e.end = prec_tok_end;
        }
    }

    SynExpr parseExpr0(){
        SynExpr ret = null;
        switch(last_tok.code){
            case sym.NULL:
                ret = new SynExpr(last_tok.beg, last_tok.end, false);
                next_tok();
                break;
            case sym.INT_LIT:
                ret = new SynExpr(last_tok.i_data, last_tok.beg, last_tok.end);
                next_tok();
                break;
            case sym.STR_LIT:
                ret = new SynExpr(last_tok.str_data, last_tok.beg, last_tok.end);
                next_tok();
                break;
            case sym.TRUE:
                ret = new SynExpr(true, last_tok.beg, last_tok.end);
                next_tok();
                break;
            case sym.FALSE:
                ret = new SynExpr(false, last_tok.beg, last_tok.end);
                next_tok();
                break;
            case sym.THIS:
                ret = new SynExpr(last_tok.beg, last_tok.end, true);
                next_tok();
                break;
            case sym.LBRACE:{
                next_tok();
                ret = parseExpr();
                assertSymb(sym.RBRACE);
                break;
            }
            case sym.NEW:{
                Location beg = last_tok.beg;
                next_tok();
                SynNtype nt = parseNtype();
                Vector<SynExpr> args = parseCall();
                ret = new SynExpr(nt, args, beg, prec_tok_end);
                break;
            }
            case sym.IDENT:{
                Location beg = last_tok.beg;
                String id = last_tok.str_data;
                next_tok();
                ret = new SynExpr(null, id, beg, prec_tok_end);
                parseCallOpt(ret);
                break;
            }
            default:
                orror(last_tok.beg, last_tok.end, "expected expr begin (" + 
                                            strForSym(sym.NULL) + 
                                            ", " + strForSym(sym.INT_LIT) + 
                                            ", " + strForSym(sym.STR_LIT) + 
                                            ", " + strForSym(sym.TRUE) + 
                                            ", " + strForSym(sym.THIS) + 
                                            ", " + strForSym(sym.LBRACE) + 
                                            ", " + strForSym(sym.NEW) + 
                                            " or " + strForSym(sym.IDENT) + 
                                            "), got " + strForSym(last_tok.code));
                break;
        }
        return ret;
    }

    SynExpr parseOp1(SynExpr ex){
        if(last_tok.code == sym.DOT){
            next_tok();
            String id = last_tok.str_data;
            assertSymb(sym.IDENT);
            SynExpr e = new SynExpr(ex, id, ex.beg, prec_tok_end);
            parseCallOpt(e);
            return parseOp1(e);
        }
        return ex;
    }

    SynExpr parseExpr1(){
        SynExpr e = parseExpr0();
        return parseOp1(e);
    }

    SynExpr parseExpr2(){
        if(last_tok.code == sym.MINUS){
            Location beg = last_tok.beg;
            next_tok();
            SynExpr e = parseExpr2();
            return new SynExpr(SynExpr.UN_MIN, e, beg, e.end);
        }
        if(last_tok.code == sym.NOT){
            Location beg = last_tok.beg;
            next_tok();
            SynExpr e = parseExpr2();
            return new SynExpr(SynExpr.UN_NOT, e, beg, e.end);
        }
        return parseExpr1();
    }

    SynExpr parseOp3(SynExpr ex){
        SynExpr e;
        switch(last_tok.code){
            case sym.TIMES:
                next_tok();
                e = parseExpr2();
                return parseOp3(new SynExpr(ex, Operator.MUL, e, ex.beg, e.end));
            case sym.SLASH:
                next_tok();
                e = parseExpr2();
                return parseOp3(new SynExpr(ex, Operator.DIV, e, ex.beg, e.end));
            case sym.MODULO:
                next_tok();
                e = parseExpr2();
                return parseOp3(new SynExpr(ex, Operator.MOD, e, ex.beg, e.end));
            default:
                break;
        }
        return ex;
    }

    SynExpr parseExpr3(){
        SynExpr e = parseExpr2();
        return parseOp3(e);
    }

    SynExpr parseOp4(SynExpr ex){
        if(last_tok.code == sym.PLUS){
            next_tok();
            SynExpr e = parseExpr3();
            return parseOp4(new SynExpr(ex, Operator.ADD, e, ex.beg, e.end));
        }
        if(last_tok.code == sym.MINUS){
            next_tok();
            SynExpr e = parseExpr3();
            return parseOp4(new SynExpr(ex, Operator.SUB, e, ex.beg, e.end));
        }
        return ex;
    }

    SynExpr parseExpr4(){
        SynExpr e = parseExpr3();
        return parseOp4(e);
    }

    SynExpr parseOp5(SynExpr ex){
        SynExpr e;
        switch(last_tok.code){
            case sym.LESS:
                next_tok();
                e = parseExpr4();
                return parseOp5(new SynExpr(ex, Operator.LESS, e, ex.beg, e.end));
            case sym.LESSEQ:
                next_tok();
                e = parseExpr4();
                return parseOp5(new SynExpr(ex, Operator.LESSEQ, e, ex.beg, e.end));
            case sym.GREATER:
                next_tok();
                e = parseExpr4();
                return parseOp5(new SynExpr(ex, Operator.GREATER, e, ex.beg, e.end));
            case sym.GREATEREQ:
                next_tok();
                e = parseExpr4();
                return parseOp5(new SynExpr(ex, Operator.GREATEREQ, e, ex.beg, e.end));
            default:
                break;
        }
        return ex;
    }

    SynExpr parseExpr5(){
        SynExpr e = parseExpr4();
        return parseOp5(e);
    }

    SynExpr parseOp6(SynExpr ex){
        if(last_tok.code == sym.EQUAL){
            next_tok();
            SynExpr e = parseExpr5();
            return parseOp6(new SynExpr(ex, Operator.EQ, e, ex.beg, e.end));
        }
        if(last_tok.code == sym.DIFFERENT){
            next_tok();
            SynExpr e = parseExpr5();
            return parseOp6(new SynExpr(ex, Operator.DIF, e, ex.beg, e.end));
        }
        return ex; 
    }

    SynExpr parseExpr6(){
        SynExpr e = parseExpr5();
        return parseOp6(e);
    }

    SynExpr parseOp7(SynExpr ex){
        if(last_tok.code == sym.AND){
            next_tok();
            SynExpr e = parseExpr6();
            return parseOp7(new SynExpr(ex, Operator.AND, e, ex.beg, e.end));
        }
        return ex; 
    }
    
    SynExpr parseExpr7(){
        SynExpr e = parseExpr6();
        return parseOp7(e);
    }

    SynExpr parseOp8(SynExpr ex){
        if(last_tok.code == sym.OR){
            next_tok();
            SynExpr e = parseExpr7();
            return parseOp8(new SynExpr(ex, Operator.OR, e, ex.beg, e.end));
        }
        return ex; 
    }
    
    SynExpr parseExpr8(){
        SynExpr e = parseExpr7();
        return parseOp8(e);
    }

    SynExpr parseOp9(SynExpr ex){
        if(last_tok.code == sym.ASIGN){
            next_tok();
            SynExpr e = parseExpr8();
            e = parseOp9(e);
            return new SynExpr(ex, e, ex.beg, e.end);
        }
        return ex; 
    }
    
    SynExpr parseExpr(){
        SynExpr e = parseExpr8();
        return parseOp9(e);
    }
}
