package analyser;

class sym {
  /* terminals */
  public static final int EOF        = 0;
  public static final int error      = 1;
  public static final int ASIGN      = 2;
  public static final int OR         = 3;
  public static final int AND        = 4;
  public static final int EQUAL      = 5;
  public static final int DIFFERENT  = 6;
  public static final int GREATER    = 7;
  public static final int GREATEREQ  = 8;
  public static final int LESS       = 9;
  public static final int LESSEQ     = 10;
  public static final int PLUS       = 11;
  public static final int MINUS      = 12;
  public static final int TIMES      = 13;
  public static final int SLASH      = 14;
  public static final int NOT        = 16;
  public static final int MODULO     = 15;
  public static final int DOT        = 17;
  public static final int LBRACE     = 18;
  public static final int RBRACE     = 19;
  public static final int LCBRACKET  = 20;
  public static final int RCBRACKET  = 21;
  public static final int LBRACKET   = 22;
  public static final int RBRACKET   = 23;
  public static final int BOOLEAN    = 24;
  public static final int CLASS      = 25;
  public static final int ELSE       = 26;
  public static final int EXTENDS    = 27;
  public static final int FALSE      = 28;
  public static final int IF         = 29;
  public static final int IMPLEMENTS = 30;
  public static final int INT        = 31;
  public static final int INTERFACE  = 32;
  public static final int NEW        = 33;
  public static final int NULL       = 34;
  public static final int PUBLIC     = 35;
  public static final int RETURN     = 36;
  public static final int STATIC     = 37;
  public static final int THIS       = 38;
  public static final int TRUE       = 39;
  public static final int VOID       = 40;
  public static final int WHILE      = 41;
  public static final int SEMICOLON  = 42;
  public static final int COMMA      = 43;
  public static final int AMPERSAND  = 44;
  public static final int IDENT      = 45;
  public static final int INT_LIT    = 46;
  public static final int STR_LIT    = 47;
  public static final int BREAK      = 48;
  public static final int CONTINUE   = 49;
  public static final int FOR        = 50;
  public static final int SWITCH     = 51;
  public static final int CASE       = 52;
  public static final int COLON      = 53;
  public static final int PACKAGE    = 54;
  public static final int IMPORT     = 55;
  public static final int FINAL      = 56;
  public static final int DEFAULT    = 57;
}

