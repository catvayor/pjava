package global;

public class Location{
    public int line;
    public int col;

    public Location(int l, int c){
        line = l;
        col = c;
    }
}
