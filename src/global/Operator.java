package global;

public class Operator{
    public static final int EQ = 0;
    public static final int DIF = 1;
    public static final int LESS = 2;
    public static final int LESSEQ = 3;
    public static final int GREATER = 4;
    public static final int GREATEREQ = 5;
    public static final int ADD = 6;
    public static final int SUB = 7;
    public static final int MUL = 8;
    public static final int DIV = 9;
    public static final int MOD = 10;
    public static final int AND = 11;
    public static final int OR = 12;
}
