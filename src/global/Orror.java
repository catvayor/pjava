package global;
import java.util.Vector;

public class Orror extends Exception{
    public static final int ERRLIST = 0;
    public static final int LEXICAL = 1;
    public static final int SYNTAX  = 2;
    public static final int TYPING  = 3;

    public static final int INTERNAL = -1;
    public static final int NOTE     = -2;

    int type;
    int exit_code;

    Vector<Orror> orrorlist;

    Location beg;
    Location end;
    String msg;

    public Orror(Vector<Orror> ol){
        if(ol.isEmpty()){
            exit_code = 2;
            type = INTERNAL;
            msg = "empty list of Orror";
            beg = new Location(-1,-1);
            end = beg;
        }
        else{
            type = ERRLIST;
            orrorlist = ol;

            exit_code = 0;
            for(Orror orr : ol)
                if(orr.get_exitCode() >= exit_code)
                    exit_code = orr.get_exitCode();
        }
    }

    public Orror(int t, Location b, Location e, String m){
        this(t,b,e,m,t == INTERNAL ? 2 : (t==NOTE? 0 : 1) );
    }

    public Orror(int t, Location b, Location e, String m, int exCode){
        type = t;
        beg = b;
        end = e;
        msg = m;
        exit_code = exCode;
    }

    String toStringNotLst(String filename){
        String ret = "File \"" + filename + "\", ";
        if(beg.line == end.line)
            ret += "line " + beg.line + ", ";
        else
            ret += "lines " + beg.line + "-" + end.line + ", ";
        ret += "characters " + beg.col + "-" + end.col + ":\n";

        switch (type){
            case INTERNAL:
                ret += "internal error : ";
                break;
            case LEXICAL:
                ret += "Lexical error : ";
                break;
            case SYNTAX:
                ret += "Syntax error : ";
                break;
            case TYPING:
                ret += "Typing error : ";
                break;
            case NOTE:
                ret += "note : ";
                break;
            default:
                ret += "error " + type + " : ";
        }
        return ret + msg;
    }

    public String toString(String filename){
        if(type == ERRLIST){
            String ret = "";
            for(Orror orr : orrorlist)
                ret += orr.toString(filename) + '\n';
            return ret;
        }
        else
            return toStringNotLst(filename);
    }

    public int get_exitCode(){
        return exit_code;
    }
}

