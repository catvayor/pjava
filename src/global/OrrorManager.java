package global;
import java.util.Vector;

public class OrrorManager{
    Vector<Orror> orrors;
    String filename;

    public OrrorManager(String _filename){
        filename = _filename;
        orrors = new Vector<Orror>();
    }

    public void orror(Orror orr){
        orrors.add(orr);
    }

    public void flush_orrors(){
        if(!orrors.isEmpty()){
            Orror global_orr = new Orror(orrors);
            System.out.print(global_orr.toString(filename));
            System.exit(global_orr.get_exitCode());
        }
    }

    public void fatal_orror(Orror orr){
        orror(orr);
        flush_orrors();
    }
}
