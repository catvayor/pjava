package syn;
import global.*;
import java.util.Vector;


public class SynClassIntf{
    public static final int CLASS = 0;
    public static final int INTF = 1;

    public Location beg_decl;
    public Location end_decl;

    public int type;

    public String ident;
    public Vector<SynParamtype> paramstype;

    public SynNtype ext;
    public Vector<SynNtype> impl;
    public Vector<SynDecl> decls;

    public Vector<SynNtype> exts;
    public Vector<SynProto> protos;

    public SynClassIntf(String id, Vector<SynParamtype> params, SynNtype e,
                     Vector<SynNtype> is, Vector<SynDecl> ds,
                     Location beg, Location end){
        type = CLASS;
        ident = id;
        paramstype = params;
        ext = e;
        impl = is;
        decls = ds;
        beg_decl = beg;
        end_decl = end;
    }
 
    public SynClassIntf(String id, Vector<SynParamtype> params, Vector<SynNtype> es,
                     Vector<SynProto> ps,
                     Location beg, Location end){
        type = INTF;
        ident = id;
        paramstype = params;
        exts = es;
        protos = ps;
        beg_decl = beg;
        end_decl = end;
    }
}

