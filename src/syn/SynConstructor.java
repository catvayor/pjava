package syn;
import global.*;
import java.util.Vector;

public class SynConstructor{
    public Location beg_decl;
    public Location end_decl;

    public String ident;
    public Vector<SynParameter> args;
    public Vector<SynStatement> body;

    public SynConstructor(String id, Vector<SynParameter> as, Vector<SynStatement> bo,
                       Location b, Location e){
        ident = id;
        args = as;
        body = bo;
        beg_decl = b;
        end_decl = e;
    }
}
