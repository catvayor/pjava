package syn;
import global.*;

public class SynDecl{
    public static final int MEMBER = 0;
    public static final int CONSTRUCTOR = 1;
    public static final int METHOD = 2;

    public Location beg_decl;
    public Location end_decl;

    public int type;

    public boolean _public;
    public boolean _static;
    public boolean _final;
    public SynType t;
    public String ident;
    public SynExpr init;

    public SynConstructor constructor;
    public SynMethod method;

    public SynDecl(boolean p, boolean s, boolean f, SynType _t, String id, 
                SynExpr ex, Location b, Location e){
        type = MEMBER;
        _public = p;
        _static = s;
        _final = f;
        t = _t;
        ident = id;
        init = ex;
        beg_decl = b;
        end_decl = e;
    }

    public SynDecl(boolean p, SynConstructor c){
        type = CONSTRUCTOR;
        _public = p;
        _static = false;
        constructor = c;
        beg_decl = c.beg_decl;
        end_decl = c.end_decl;
    }

    public SynDecl(SynMethod m){
        type = METHOD;
        _public = m.proto._public;
        _static = m.proto._static;
        method = m;
        beg_decl = m.beg_decl;
        end_decl = m.end_decl;
    }
}
