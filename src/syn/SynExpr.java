package syn;
import java.util.Vector;
import global.*;

public class SynExpr{
    public static final int NULL = 0;
    public static final int ASIGN = 1;
    public static final int UN_NOT = 2;
    public static final int UN_MIN = 3;
    public static final int OP = 4;
    public static final int INT_LIT = 5;
    public static final int STR_LIT = 6;
    public static final int BOOL_LIT = 7;
    public static final int THIS = 8;
    public static final int CONSTRUCT = 9;
    public static final int CALL = 10;
    public static final int VAR = 11;
    
    public static final int NO_EXPR = -1;

    public Location beg;
    public Location end;

    public int type;

    public SynExpr e1;
    public int op;
    public SynExpr e2;

    public int int_lit;
    public String str_lit;
    public boolean bool_lit;
    
    public SynNtype nt;
    public Vector<SynExpr> args;
    public String ident;

    public SynExpr(){
        type = NO_EXPR;
        beg = new Location(-1,-1);
        end = new Location(-1,-1);
    }

    public SynExpr(Location b, Location e, boolean tn){//tn true iff this, else null
        type = tn ? THIS : NULL;
        beg = b;
        end = e;
    }

    public SynExpr(SynExpr a, SynExpr ex, Location b, Location e){
        type = ASIGN;
        e1 = a;
        e2 = ex;
        beg = b;
        end = e;
    }

    public SynExpr(int o, SynExpr ex, Location b, Location e){
        type = o;
        e1 = ex;
        beg = b;
        end = e;
    }

    public SynExpr(SynExpr _e1, int o, SynExpr _e2, Location b, Location e){
        type = OP;
        e1 = _e1;
        op = o;
        e2 = _e2;
        beg = b;
        end = e;
    }

    public SynExpr(int i, Location b, Location e){
        type = INT_LIT;
        beg = b;
        end = e;
        int_lit = i;
    }

    public SynExpr(String str, Location b, Location e){
        type = STR_LIT;
        beg = b;
        end = e;
        str_lit = str;
    }

    public SynExpr(boolean bo, Location b, Location e){
        type = BOOL_LIT;
        beg = b;
        end = e;
        bool_lit = bo;
    }

    public SynExpr(SynNtype _nt, Vector<SynExpr> as, Location b, Location e){
        type = CONSTRUCT;
        beg = b;
        end = e;
        args = as;
        nt = _nt;
    }

    public SynExpr(SynExpr a, String id, Vector<SynExpr> as, Location b, Location e){
        type = CALL;
        beg = b;
        end = e;
        e1 = a;
        ident = id;
        args = as;
    }

    public SynExpr(SynExpr a, String id, Location b, Location e){
        type = VAR;
        beg = b;
        end = e;
        e1 = a;
        ident = id;
    }
}
