package syn;
import java.util.Vector;
import global.*;

public class SynMethod{
    public Location beg_decl;
    public Location end_decl;

    public SynProto proto;
    public Vector<SynStatement> body;

    public SynMethod(SynProto p, Vector<SynStatement> b){
        proto = p;
        body = b;
        beg_decl = p.beg;
        end_decl = p.end;
    }
}
