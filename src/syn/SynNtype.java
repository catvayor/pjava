package syn;
import global.*;
import java.util.Vector;

public class SynNtype{
    public Location beg;
    public Location end;

    public String ident;
    public Vector<SynNtype> gen_param;
    public SynNtype(String id, Vector<SynNtype> param,
                 Location b, Location e){
        ident = id;
        gen_param = param;
        beg = b;
        end = e;
    }
}
