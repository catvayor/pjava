package syn;
import global.*;

public class SynParameter{
    public Location beg;
    public Location end;

    public SynType type;
    public String ident;

    public SynParameter(SynType t, String id, Location b, Location e){
        beg = b;
        end = e;
        type = t;
        ident = id;
    }
}
