package syn;
import global.*;
import java.util.Vector;

public class SynParamtype{
    public Location beg_decl;
    public Location end_decl;

    public String ident;
    public Vector<SynNtype> ext_nt;

    public SynParamtype(String id, Vector<SynNtype> ext, 
                     Location b, Location e){
        ident = id;
        ext_nt = ext;
        beg_decl = b;
        end_decl = e;
    }
}
