package syn;
import global.*;
import java.util.Vector;

public class SynProto{
    public Location beg;
    public Location end;

    public boolean _public;
    public boolean _static;
    public SynType ret;
    public String ident;
    public Vector<SynParameter> params;

    public SynProto(boolean p, boolean s, SynType t, String id, Vector<SynParameter> p_l,
                 Location b, Location e){
        _public = p;
        _static = s;
        ret = t;
        ident = id;
        params = p_l;
        beg = b;
        end = e;
    }
}
