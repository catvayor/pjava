package syn;
import java.util.Vector;
import global.*;

public class SynStatement{
    public static final int EMPTY     = 0;
    public static final int EVAL      = 2;
    public static final int DECL      = 3;
    public static final int IF        = 4;
    public static final int SWITCH    = 5;
    public static final int FOR       = 6;
    public static final int FORIT     = 7;
    public static final int WHILE     = 8;
    public static final int BLOCK     = 9;
    public static final int BREAK     = 10;
    public static final int CONTINUE  = 11;
    public static final int RETURN    = 12;

    public Location beg;//not here for block, if, while
    public Location end;

    public int type;

    public SynExpr e;
    public SynType t;
    public String ident;

    public SynExpr init;
    public SynExpr inc;
    public SynStatement body;
    public SynStatement belse;
    public Vector<SynStatement> stmts;

    public Vector<Pair<SynExpr,Vector<SynStatement>>> cases; 
    public Vector<SynStatement> defaultCase; 

    public SynStatement(){
        type = EMPTY;
    }
    
    public SynStatement(boolean r, SynExpr _e, Location b_, Location e_){
        type = r ? RETURN : EVAL;
        e = _e;
        beg = b_;
        end = e_;
    }

    public SynStatement(SynType _t, String id, SynExpr _e, Location b_, Location e_){
        type = DECL;
        t = _t;
        ident = id;
        e = _e;
        beg = b_;
        end = e_;
    }

    public SynStatement(SynExpr cond, SynStatement b){
        type = WHILE;
        e = cond;
        body = b;
    }

    public SynStatement(SynExpr cond, SynStatement t, SynStatement f){
        type = IF;
        e = cond;
        body = t;
        belse = f;
    }

    public SynStatement(Vector<SynStatement> _stmts){
        type = BLOCK;
        stmts = _stmts;
    }

    public SynStatement(int t, Location b, Location e){
        type = t;
        beg = b;
        end = e;
    }

    public SynStatement(SynType _t, String id, SynExpr i, 
            SynExpr c, SynExpr plus, SynStatement b){
        type = FOR;
        t = _t;
        ident = id;
        init = i;
        e = c;
        inc = plus;
        body = b;
    }

    public SynStatement(SynType _t, String id, SynExpr _e, SynStatement b){
        type = FORIT;
        t = _t;
        ident = id;
        e = _e;
        body = b;
    }

    public SynStatement(SynExpr c, Vector<Pair<SynExpr,Vector<SynStatement>>> cs, 
                     Vector<SynStatement> dc){
        type = SWITCH;
        e = c;
        cases = cs;
        defaultCase = dc;
    }
}
