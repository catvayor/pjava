package syn;
import global.*;

public class SynType{
    public static final int NTYPE = 0;
    public static final int BOOL  = 1;
    public static final int INT   = 2;
    public static final int VOID  = 3;
    public static final int ARR   = 4;

    public Location beg;
    public Location end;

    public int type;
    public SynNtype nt;

    public SynType _t;

    public SynType(int t, Location b, Location e){
        type = t;
        beg = b;
        end = e;
    }
 
    public SynType(SynNtype _nt, Location b, Location e){
        type = NTYPE;
        nt = _nt;
        beg = b;
        end = e;
    }
    
    public SynType(SynType t, Location b, Location e){
        type = ARR;
        _t = t;
        beg = b;
        end = e;
    }
}

