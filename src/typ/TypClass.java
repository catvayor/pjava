package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypClass{
    public SynClassIntf syn;

    public Vector<String> params_name;//for order
    public Hashtable<String, TypType> params;
    public TypType ext;
    public Vector<TypType> impls;

    public Hashtable<String, TypType> fields;
    public Hashtable<String, TypType> static_fields;//TODO : finals
    public TypConstructor constructor;
    public Hashtable<String, TypMethod> methods;
    public Hashtable<String, TypMethod> static_methods;

    public Hashtable<String, SynExpr> syn_static_init;
    public Hashtable<String, TypExpr> static_init;

    public int size;
    public Hashtable<String, Integer> fields_idx;

    public int arity(){
        return params_name.size();
    }

    public TypClass(SynClassIntf ci){
        syn = ci;
        params_name = new Vector<String>();
        if(syn.paramstype == null)
            syn.paramstype = new Vector<SynParamtype>();
        for(SynParamtype pt : syn.paramstype)
            params_name.add(pt.ident);
    }

    public void build(TypEnvironment env){
        params = new Hashtable<String, TypType>();
        for(SynParamtype pt : syn.paramstype){
            TypType pc = params.get(pt.ident);
            if(pc == null)
                params.put(pt.ident, new TypType(pt.ident, pt.beg_decl, pt.end_decl));
            else{
                env.typ_orror(pt.beg_decl, pt.end_decl, pt.ident + " already defined");
                env.note_on_orror(pc.beg, pc.end, "first definition here");
            }
        }
        for(SynParamtype pt : syn.paramstype){
            Vector<TypType> exts = params.get(pt.ident).over_types;
            if(pt.ext_nt != null){//never empty if exist
                SynNtype nt = pt.ext_nt.get(0);
                TypType t = new TypType(nt, env, params);
                if(t.type != TypType.CLASS && t.type != TypType.PARAM){
                    exts.add(new TypType(TypEnvironment.OBJECT, env, params));
                }
                if(t.ident.equals("String"))
                    env.typ_orror(nt.beg, nt.end, "can't inherit from String");
                exts.add(t);
                for(int i = 1; i < pt.ext_nt.size(); ++i){
                    nt = pt.ext_nt.get(i);
                    t = new TypType(nt, env, params);
                    exts.add(t);
                    if(t.type != TypType.INTF)
                        env.typ_orror(nt.beg, nt.end, "expected interface");
                }
            }
        }

        if(syn.ext != null){
            ext = new TypType(syn.ext, env, params);
            if(ext.type != TypType.CLASS)
                env.typ_orror(syn.ext.beg, syn.ext.end, "expected class");
            if(ext.ident.equals("String"))
                env.typ_orror(syn.ext.beg, syn.ext.end, "can't inherit from String");
        } else
            ext = new TypType(TypEnvironment.OBJECT, env, params);

        impls = new Vector<TypType>();
        if(syn.impl != null)
            for(SynNtype nt : syn.impl){
                TypType impl = new TypType(nt, env, params);
                impls.add(impl);
                if(impl.type != TypType.INTF)
                    env.typ_orror(nt.beg, nt.end, "expected interface");
            }
    }

    //same as Environment
    String check_inherit_impl(String id, Hashtable<String, Integer> currentState, 
                              TypEnvironment env){
        TypType t = params.get(id);
        Integer state = currentState.get(id);
        if(state.equals(1)){
            env.typ_orror(t.beg, t.end, "inheritance loop on " + id);
            return id;
        }
        if(state.equals(2))
            return null;

        currentState.put(id, 1);
        for(TypType ext : t.over_types){
            if(ext.type != TypType.PARAM)
                continue;
            String ret = check_inherit_impl(ext.ident, currentState, env);
            if(ret != null){
                env.note_on_orror(t.beg, t.end, "which is inherited by " + id);
                if(!ret.equals(id)){
                    currentState.put(id, 2);
                    return ret;
                }
            }
        }
        currentState.put(id, 2);
        return null;
    }
    public void check_arg_inheritance(TypEnvironment env){
        Hashtable<String, Integer> state = new Hashtable<String, Integer>();
        for(String id : params_name)
            state.put(id, 0);
        for(String id : params_name)
            check_inherit_impl(id, state, env);
    }

    public void build_over_types(TypEnvironment env){
        ext.build_over_types(env);
        for(TypType t : impls)
            t.build_over_types(env);
        for(TypType t : params.values())
            t.build_over_types(env);
    }

    public Hashtable<String, TypType> substTable(Vector<TypType> spec){
        //spec and params assumed of same size
        Hashtable<String, TypType> ret = new Hashtable<String, TypType>();
        for(int i = 0; i < arity(); ++i)
            ret.put(params_name.get(i), spec.get(i));
        return ret;
    }

    public void checkValid(TypEnvironment env){
        ext.checkValid(env);
        for(TypType t : impls)
            t.checkValid(env);
        for(TypType t : params.values())
            for(TypType p : t.over_types)
                p.checkValid(env);
    }

    public void buildMembers(TypEnvironment env){
        fields = new Hashtable<String, TypType>();
        static_fields = new Hashtable<String, TypType>();
        syn_static_init = new Hashtable<String, SynExpr>();
        methods = new Hashtable<String, TypMethod>();
        static_methods = new Hashtable<String, TypMethod>();
        Hashtable<String, SynDecl> mdecl_done = new Hashtable<String, SynDecl>();
        Hashtable<String, SynDecl> fdecl_done = new Hashtable<String, SynDecl>();
        for(SynDecl d : syn.decls){
            switch(d.type){
                case SynDecl.MEMBER:{
                    SynDecl dc = fdecl_done.get(d.ident);
                    if(dc == null){
                        TypType ty = TypType.fromT(d.t, env, params);
                        if(ty.isSame(TypType.void_type))
                            env.typ_orror(d.beg_decl, d.end_decl, "cannot have void fields");
                        if(d._static){
                            if(!params.isEmpty())
                                env.typ_orror(d.beg_decl, d.end_decl, 
                                        "static variable illegal in generic classes");
                            static_fields.put(d.ident, ty);
                            syn_static_init.put(d.ident, d.init);
                        }else
                            fields.put(d.ident, ty);
                        fdecl_done.put(d.ident, d);
                    } else {
                        env.typ_orror(d.beg_decl, d.end_decl, 
                                "member " + d.ident + " already defined");
                        env.note_on_orror(dc.beg_decl, dc.end_decl,
                                "first definition here");
                    }
                    break;
                }
                case SynDecl.CONSTRUCTOR: {
                    if(constructor != null){
                        env.typ_orror(d.beg_decl, d.end_decl, 
                                "second definition of constructor of " + syn.ident);
                        env.note_on_orror(constructor.syn.beg_decl,
                                          constructor.syn.end_decl,
                                          "first definition here");
                    }
                    constructor = new TypConstructor(d.constructor, syn.ident, env, params);
                    break; 
                }
                case SynDecl.METHOD:{ 
                    SynDecl dc = mdecl_done.get(d.method.proto.ident);
                    if(dc == null){
                        if(d._static)
                            static_methods.put(d.method.proto.ident, 
                                    new TypMethod(d.method, env, params));
                        else 
                            methods.put(d.method.proto.ident, 
                                    new TypMethod(d.method, env, params));
                        mdecl_done.put(d.method.proto.ident, d);
                    } else {
                        env.typ_orror(d.beg_decl, d.end_decl, 
                                "member " + d.method.proto.ident + " already defined");
                        env.note_on_orror(dc.beg_decl, dc.end_decl,
                                "first definition here");
                    }
                    break; 
                }
            }
        }

        if(constructor == null){
        constructor = new TypConstructor(
                        new SynConstructor(syn.ident, 
                            new Vector<SynParameter>(), new Vector<SynStatement>(), 
                            syn.beg_decl, syn.end_decl),
                        syn.ident, env, params);
        }
    }

    public void buildBodies(TypEnvironment env){
        SynNtype ment = new SynNtype(syn.ident, null, syn.beg_decl, syn.end_decl);
        if(!params_name.isEmpty()){
            ment.gen_param = new Vector<SynNtype>();
            for(String pid : params_name)
                ment.gen_param.add(new SynNtype(pid, null, syn.beg_decl, syn.end_decl));
        }
        TypType me = TypType.fromT(ment, env, params);
        constructor.buildBody(env, params, me);
        static_init = new Hashtable<String, TypExpr>();
        for(TypMethod m : methods.values())
            m.buildBody(env, params, me);
        for(TypMethod m : static_methods.values())
            m.buildBody(env, params, me);
        for(String id : syn_static_init.keySet()){
            TypExpr i = new TypExpr(syn_static_init.get(id),
                            env, params, me, null, false, true);
            static_init.put(id, i);
            if(!i.type.isSubtypeOf(static_fields.get(id), params))
                env.typ_orror(i.beg, i.end, "expected expression of type " +
                                static_fields.get(id) + " got " + i.type); 
        }
        
    }
    
    public void checkProtosConflict(TypEnvironment env){
        {
            TypClass cl = env.classes.get(ext.ident);
            TypConstructor c = cl.constructor;
            if(!c.arguments.isEmpty()){
                env.typ_orror(syn.beg_decl, syn.end_decl, 
                        "inheritance from class with no constructor without "+
                        "arguments is not supported");
                env.note_on_orror(c.syn.beg_decl, c.syn.end_decl,
                        "inherited constructor");
            }
        }
        SynNtype ment = new SynNtype(syn.ident, null, syn.beg_decl, syn.end_decl);
        if(!params_name.isEmpty()){
            ment.gen_param = new Vector<SynNtype>();
            for(String pid : params_name)
                ment.gen_param.add(new SynNtype(pid, null, syn.beg_decl, syn.end_decl));
        }
        TypType me = TypType.fromT(ment, env, params);
        
        Hashtable<String, TypProto> eff_protos = me.checkImplementsConflict(env);

        for(TypType param : params.values())
            param.checkImplementsConflict(env);

        Hashtable<String, TypProto> eff_methods = ext.listClProtos(env);
        for(String mid : methods.keySet()){
            TypProto ext_p = eff_methods.get(mid);
            TypProto p = methods.get(mid);
            eff_methods.put(mid, p);

            if(ext_p == null)
                continue;

            ext_p.checkSpecialisation(env, p);
        }

        for(String pid : eff_protos.keySet()){
            TypProto p = eff_protos.get(pid);
            TypProto m = eff_methods.get(pid);
            if(m == null){
                env.typ_orror(syn.beg_decl, syn.end_decl, 
                        "missing prototype implementation in " + 
                        syn.ident + " : " + pid);
                env.note_on_orror(p.beg, p.end, "missing prototype");
                continue;
            }
            if(!m._public){
                env.typ_orror(m.beg, m.end, 
                        "this methods should be public");
                env.note_on_orror(p.beg, p.end, 
                        "because this prototype is implemented by this method");
                env.note_on_orror(syn.beg_decl, syn.end_decl,
                        "implementation occurs in this class");
            }
            p.checkSpecialisation(env, m);
        }
    }

    public void calcIdx(TypEnvironment env){
        if(fields_idx != null)
            return;
        if(syn.ident.equals("Object")){
            fields_idx = new Hashtable<String, Integer>();
            size = 0;
            return;
        }
        TypClass extd = env.classes.get(ext.ident);
        extd.calcIdx(env);
        fields_idx = new Hashtable<String, Integer>(extd.fields_idx);
        size = extd.size;
        for(String f : fields.keySet()){
            size += 8;
            fields_idx.put(f, size);
        }
    }
};
