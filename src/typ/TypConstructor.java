package typ;
import java.util.Vector;
import java.util.Hashtable;
import global.*;
import syn.*;

public class TypConstructor{
    SynConstructor syn;

    public Vector<Pair<String, TypType> > arguments;
    public TypScope body;

    public TypConstructor(SynConstructor c, String class_ident, 
                       TypEnvironment env, Hashtable<String, TypType> params){
        syn = c;
        if(!syn.ident.equals(class_ident))
            env.typ_orror(syn.beg_decl, syn.end_decl, 
                    "expected constructor of " + class_ident + 
                    " found constructor of " + syn.ident);

        arguments = new Vector<Pair<String, TypType> >();
        Hashtable<String, SynParameter> arg_params = new Hashtable<String, SynParameter>();
        for(SynParameter arg : c.args){
            arguments.add(new Pair<String, TypType>(
                        arg.ident,
                        TypType.fromT(arg.type, env, params)));
            SynParameter param = arg_params.get(arg.ident);
            if(param != null){
                env.typ_orror(arg.beg, arg.end, arg.ident + " already defined");
                env.typ_orror(param.beg, param.end, "first definition here");
            }
            arg_params.put(arg.ident, arg);
        }
    }

    public void buildBody(TypEnvironment env, Hashtable<String, TypType> params, TypType cl){
        body = new TypScope(syn.body, env, params, cl, TypType.void_type, arguments, false);
    }
}

