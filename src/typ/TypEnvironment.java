package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypEnvironment{
    Vector<Orror> orrors;
    OrrorManager orr_man;

    public void typ_orror(Location beg, Location end, String msg){
        orr_man.orror(new Orror(Orror.TYPING, beg, end, msg));
    }
    public void note_on_orror(Location beg, Location end, String msg){
        orr_man.orror(new Orror(Orror.NOTE, beg, end, msg));
    }

    public void internal_orror(Location beg, Location end, String msg){
        orr_man.orror(new Orror(Orror.INTERNAL, beg, end, msg));
    }
    
    public void internal_orror(String msg){
        internal_orror(new Location(-1,-1), new Location(-1,-1), msg);
    }

    //Ntype for Object
    public static final SynNtype OBJECT = 
        new SynNtype("Object", null, new Location(-1,-1), new Location(-1,-1));

    public Hashtable<String, TypInterface> interfaces;
    public Hashtable<String, TypClass> classes;
    public Hashtable<String, Integer> class_desc;

    public Hashtable<String, Vector<Pair<TypClass,String>>> expr_dep;

    Location loc_beg(String id){
        TypInterface intf = interfaces.get(id);
        TypClass c = classes.get(id);
        if(intf != null)
            return intf.syn.beg_decl;
        else if(c != null)
            return c.syn.beg_decl;
        else{
            internal_orror("searched location for : " + id + " which was not found");
            return new Location(-1,-1);
        }
    }

    Location loc_end(String id){
        TypInterface intf = interfaces.get(id);
        TypClass c = classes.get(id);
        if(intf != null)
            return intf.syn.end_decl;
        else if(c != null)
            return c.syn.end_decl;
        else{
            internal_orror("searched location for : " + id + " which was not found");
            return new Location(-1,-1);
        }
    }

    public TypEnvironment(OrrorManager man){ 
        orr_man = man;
        orrors = new Vector<Orror>();
        interfaces = new Hashtable<String, TypInterface>();
        classes = new Hashtable<String, TypClass>();

        Location noLoc = new Location(-1,-1);

        //class Object
        {
            add_classintf(new 
                    SynClassIntf("Object", null, null, null, new Vector<SynDecl>(),
                              noLoc, noLoc));
        }

        //class String
        {
            Vector<SynDecl> String_decls = new Vector<SynDecl>();
            Vector<SynParameter> string_eq_proto_args = new Vector<SynParameter>();
            string_eq_proto_args.add(
                    new SynParameter(new SynType(new SynNtype("String", null, 
                                noLoc, noLoc), 
                            noLoc, noLoc), "other",
                            noLoc, noLoc));
            SynProto String_eq_proto = new SynProto(
                    true, false, new SynType(SynType.BOOL, noLoc, noLoc),
                    "equals", string_eq_proto_args, 
                    noLoc, noLoc);
            Vector<SynStatement> equals_body = new Vector<SynStatement>();
            equals_body.add(new SynStatement(true, new SynExpr(true, 
                                noLoc, noLoc),
                        noLoc, noLoc));
            String_decls.add(new SynDecl(new SynMethod(
                                String_eq_proto, equals_body)));
            String_decls.add(new SynDecl(true, false, false, new SynType(SynType.INT,
                            noLoc, noLoc), "size", new SynExpr(),
                        noLoc, noLoc));
            String_decls.add(new SynDecl(true, false, false, new SynType(SynType.INT,
                            noLoc, noLoc), "str_dummy", new SynExpr(),
                        noLoc, noLoc));
            add_classintf(new 
                    SynClassIntf("String", null, null, null, String_decls,
                              noLoc, noLoc));
        }

        //class System & System.out
        {
            Vector<SynDecl> System_decls = new Vector<SynDecl>();
            SynNtype sysout_nt = new SynNtype("System.out", null, noLoc, noLoc);
            System_decls.add(new SynDecl(true, true, false, 
                        new SynType(sysout_nt,noLoc, noLoc), "out", new SynExpr(
                                sysout_nt, new Vector<SynExpr>(), noLoc, noLoc),
                        noLoc, noLoc));
            System_decls.add(new SynDecl(true, true, false, 
                        new SynType(sysout_nt,noLoc, noLoc), "exit_point", new SynExpr(),
                        noLoc, noLoc));
            Vector<SynParameter> sys_exit_args = new Vector<SynParameter>();
            sys_exit_args.add(
                    new SynParameter(new SynType(SynType.INT, 
                            noLoc, noLoc), "code",
                            noLoc, noLoc));
            SynProto sys_exit_proto = new SynProto(
                    true, true, new SynType(SynType.VOID, noLoc, noLoc),
                    "exit", sys_exit_args, 
                    noLoc, noLoc);
            System_decls.add(new SynDecl(new SynMethod(
                                sys_exit_proto, new Vector<SynStatement>())));
            add_classintf(new 
                    SynClassIntf("System", null, null, null, System_decls,
                              noLoc, noLoc));

            Vector<SynDecl> System_out_decls = new Vector<SynDecl>();
            Vector<SynParameter> sysout_print_args = new Vector<SynParameter>();
            sysout_print_args.add(
                    new SynParameter(new SynType(new SynNtype("String", null, noLoc, noLoc), 
                            noLoc, noLoc), "msg",
                            noLoc, noLoc));
            SynProto sysout_print_proto = new SynProto(
                    true, false, new SynType(SynType.VOID, noLoc, noLoc),
                    "print", sysout_print_args, 
                    noLoc, noLoc);
            System_out_decls.add(new SynDecl(new SynMethod(
                                sysout_print_proto, new Vector<SynStatement>())));
            SynProto sysout_println_proto = new SynProto(
                    true, false, new SynType(SynType.VOID, noLoc, noLoc),
                    "println", sysout_print_args, 
                    noLoc, noLoc);
            System_out_decls.add(new SynDecl(new SynMethod(
                                sysout_println_proto, new Vector<SynStatement>())));
            add_classintf(new 
                    SynClassIntf("System.out", null, null, null, System_out_decls,
                              noLoc, noLoc));
        }

        //iterable & iterator interfaces
        {
            Vector<SynProto> iterator_protos = new Vector<SynProto>();
            iterator_protos.add(new SynProto(false, false, 
                        new SynType(SynType.BOOL, noLoc, noLoc),
                        "hasNext", new Vector<SynParameter>(), 
                        noLoc, noLoc));
            iterator_protos.add(new SynProto(false, false,
                        new SynType(new SynNtype("E", null,
                                noLoc, noLoc), 
                            noLoc, noLoc),
                        "next", new Vector<SynParameter>(), 
                        noLoc, noLoc));
            Vector<SynParamtype> it_param = new Vector<SynParamtype>();
            it_param.add(new SynParamtype("E", null, noLoc, noLoc));
            add_classintf(new SynClassIntf("Iterator", it_param, null, iterator_protos,
                        noLoc, noLoc));
        
            Vector<SynProto> iterable_protos = new Vector<SynProto>();
            Vector<SynNtype> iterator_param = new Vector<SynNtype>();
            iterator_param.add(new SynNtype("E", null, 
                        noLoc, noLoc));
            iterable_protos.add(new SynProto(false, false,
                        new SynType(new SynNtype("Iterator", iterator_param,
                                noLoc, noLoc), 
                            noLoc, noLoc),
                        "iterator", new Vector<SynParameter>(), 
                        noLoc, noLoc));
            add_classintf(new SynClassIntf("Iterable", it_param, null, iterable_protos,
                        noLoc, noLoc));
        }
    }

    public void add_classintf(SynClassIntf ci){
        if(interfaces.get(ci.ident) != null){
            TypInterface intf = interfaces.get(ci.ident);
            typ_orror(ci.beg_decl, ci.end_decl, ci.ident + " already defined");
            note_on_orror(intf.syn.beg_decl, intf.syn.end_decl, "first definition here");
        } else if (classes.get(ci.ident) != null){
            TypClass c = classes.get(ci.ident);
            typ_orror(ci.beg_decl, ci.end_decl, ci.ident + " already defined");
            note_on_orror(c.syn.beg_decl, c.syn.end_decl, "first definition here");
        } else{
            if(ci.type == SynClassIntf.CLASS)
                classes.put(ci.ident, new TypClass(ci));
            else
                interfaces.put(ci.ident, new TypInterface(ci));
        }
    }

    public void build_params(){
        for(TypClass c : classes.values())
            c.build(this);
        for(TypInterface intf : interfaces.values())
            intf.build(this);
    }

    //return != null if has loop, hashtable map to 0 if not seen, 1 if inprogress, 2 else;
    //return value used to generate error
    String check_inherit_impl(String id, Hashtable<String, Integer> currentState){ 
        Integer state = currentState.get(id);
        if(state.equals(1)){
            typ_orror(loc_beg(id), loc_end(id), "inheritance loop on " + id);
            return id;
        }
        if(state.equals(2))
            return null;

        currentState.put(id, 1);
        TypClass c = classes.get(id);
        if(c != null){
            String ret = check_inherit_impl(c.ext.ident, currentState);
            if(ret != null){
                note_on_orror(c.syn.beg_decl, c.syn.end_decl, 
                        "which is inherited by " + id);
                if(ret.equals(id))
                    ret = null;
            }
            currentState.put(id, 2);
            return ret;
        }
        TypInterface intf = interfaces.get(id);
        if(intf != null){
            for(TypType ext : intf.exts){
                String ret = check_inherit_impl(ext.ident, currentState);
                if(ret != null){
                    note_on_orror(intf.syn.beg_decl, intf.syn.end_decl, 
                            "which is inherited by " + id);
                    if(!ret.equals(id)){
                        currentState.put(id, 2);
                        return ret;
                    }
                }
            }
            currentState.put(id, 2);
            return null;
        }
        internal_orror("inheritance openned " + id + " which is not declared");
        return null;
    }
    public void check_inheritance(){
        Hashtable<String, Integer> state = new Hashtable<String, Integer>();
        for(String id : classes.keySet())
            state.put(id, 0);
        for(String id : interfaces.keySet())
            state.put(id, 0);

        state.put("Object", 2);//avoid error on Object (in practice Object inherit from itself)

        for(String id : state.keySet())
            check_inherit_impl(id, state);

        for(TypClass c : classes.values())
            c.check_arg_inheritance(this);
        for(TypInterface intf : interfaces.values())
            intf.check_arg_inheritance(this);
    }

    public void build_over_types(){
        for(TypClass c : classes.values())
            c.build_over_types(this);
        for(TypInterface intf : interfaces.values())
            intf.build_over_types(this);

        TypType.str_type = TypType.fromT(new SynNtype("String", null, 
                                        new Location(-1, -1),
                                        new Location(-1, -1)), 
                                   this, new Hashtable<String, TypType>() );
    }

    public void checkValidity(){
        for(TypClass c : classes.values())
            c.checkValid(this);
        for(TypInterface intf : interfaces.values())
            intf.checkValid(this);
    }

    public void buildProtos(){
        for(TypClass c : classes.values()){
            c.buildMembers(this);
        }
        for(TypInterface intf : interfaces.values()){
            intf.buildProtos(this); 
        }
    }

    public void buildBodies(){
        for(TypClass c : classes.values())
            c.buildBodies(this);
    }

    public void checkProtosConflict(){
        for(TypInterface intf : interfaces.values())
            intf.checkProtosConflict(this);
        for(TypClass c : classes.values())
            c.checkProtosConflict(this);
    }


    void addReq(Vector<Pair<TypClass, String>> dep, TypExpr expr){
        switch(expr.opType){
            case TypExpr.LIT_I:
            case TypExpr.LIT_B:
            case TypExpr.LIT_S:
            case TypExpr.THIS:
            case TypExpr.CLVAR:
            case TypExpr.NULL:
                break;
            
            case TypExpr.CALL:
                addReq(dep, expr.from);
            case TypExpr.STCALL:
            case TypExpr.BUILD:
                for(TypExpr e : expr.args)
                    addReq(dep, e);
                break;
            
            case TypExpr.OP:
                addReq(dep, expr.e2);
            case TypExpr.UMIN:
            case TypExpr.NOT:
                addReq(dep, expr.e1);
                break;
            
            case TypExpr.STVAR:
                dep.add(new Pair<TypClass, String>(
                            classes.get(expr.from.type.ident),
                            expr.ident));
                break;
        }
    }
    String check_stinit_impl(TypClass cl, String id, Hashtable<String, Integer> currentState){ 
        String fullid = cl.syn.ident + "." + id;
        Integer state = currentState.get(fullid);
        if(state.equals(1)){
            typ_orror(cl.static_fields.get(id).beg, 
                      cl.static_fields.get(id).end, "static requirement loop on " + fullid);
            return fullid;
        }
        if(state.equals(2))
            return null;

        currentState.put(fullid, 1);
        Vector<Pair<TypClass, String>> st_req = new Vector<Pair<TypClass, String>>();
        addReq(st_req, cl.static_init.get(id));
        expr_dep.put(fullid, st_req);        

        for(Pair<TypClass, String> dep : st_req){
            String ret = check_stinit_impl(dep.first, dep.second, currentState);
            if(ret != null){
                note_on_orror(cl.static_fields.get(id).beg, 
                              cl.static_fields.get(id).end, 
                              "which require of " + fullid);
                if(!ret.equals(fullid))
                    return ret;
            }
            currentState.put(fullid, 2);
        }
        return null;
    }
    public void check_stinit(){
        expr_dep = new Hashtable<String, Vector<Pair<TypClass,String>>>();
        Hashtable<String, Integer> state = new Hashtable<String, Integer>();
        for(TypClass cl : classes.values())
            for(String id : cl.static_fields.keySet())
                state.put(cl.syn.ident + "." + id, 0);

        for(TypClass cl : classes.values())
            for(String id : cl.static_fields.keySet())
                check_stinit_impl(cl, id, state);
    }

    public void check_main(){
        TypClass cl = classes.get("Main");
        if(cl == null){
            typ_orror(new Location(0,0), new Location(0,0),
                    "program must provide a 'Main' class");
            return;
        }
        if(!cl.params.isEmpty())
            typ_orror(cl.syn.beg_decl, cl.syn.end_decl, 
                    "Main should not be generic");
        TypProto m_proto = cl.static_methods.get("main");
        if(m_proto == null){
            typ_orror(cl.syn.beg_decl, cl.syn.end_decl, 
                    "Main should provide a public static method void main(String[])");
            return;
        }
        if(!m_proto.retType.isSame(TypType.void_type))
            typ_orror(m_proto.beg, m_proto.end, 
                    "Main.main should return void");
        if(m_proto.arguments.size() != 1){
            typ_orror(m_proto.beg, m_proto.end,
                    "Main.main should take exactly one argument");
            return;
        }
        TypType arg_t = m_proto.arguments.get(0).second;
        if(arg_t.type != TypType.ARRT || !arg_t.arrT.isSame(TypType.str_type))
            typ_orror(m_proto.beg, m_proto.end,
                    "Main.main should take a String[]");
    }

    public void calcFieldsIdx(){
        for(TypClass c : classes.values())
            c.calcIdx(this); 
    }
}

