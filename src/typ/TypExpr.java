package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypExpr{
    public Location beg;
    public Location end;

    public static final int LIT_I  = 0;
    public static final int LIT_B  = 1;
    public static final int LIT_S  = 2;
    public static final int THIS   = 3;
    public static final int BUILD  = 4;
    public static final int CALL   = 5;
    public static final int STCALL = 6;
    public static final int VAR    = 7;
    public static final int STVAR  = 8;
    public static final int CLVAR  = 9;

    public static final int NULL  = 10;
    public static final int ASIGN = 11;
    public static final int NOT   = 12;
    public static final int UMIN  = 13;
    public static final int OP    = 14;

    public static final int NO_EXPR  = -1;
    
    public int opType;

    public int lit_i_val;
    public boolean lit_b_val;
    public String lit_s_val;

    public boolean notExpr;
    public TypExpr from;
    public String ident;

    public Vector<TypExpr> args;

    public TypExpr e1;
    public TypExpr e2;
    public int op;

    public TypType type;

    void cpy(TypExpr other){
        opType = other.opType;

        lit_i_val = other.lit_i_val;
        lit_b_val = other.lit_b_val;
        lit_s_val = other.lit_s_val;
        
        from = other.from;
        notExpr = other.notExpr;
        ident = other.ident;

        args = other.args;

        e1 = other.e1;
        e2 = other.e2;
        op = other.op;
        
        type = other.type;
    }

    public TypExpr(SynExpr synt, TypEnvironment env,
            Hashtable<String, TypType> params, TypType cl, TypScope scope,
            boolean allowNoExpr, boolean _static){
        beg = synt.beg;
        end = synt.end;
        notExpr = false;
        switch(synt.type){
        case SynExpr.NO_EXPR:
            opType = NO_EXPR;
            type = TypType.orror_type;
            break;
        case SynExpr.NULL:
            opType = NULL;
            type = TypType.null_type;
            break;
        case SynExpr.ASIGN:
            opType = ASIGN;
            if(scope == null)
                env.typ_orror(beg, end, "can not use = in static variable initialisation");
            if(synt.e1.type != SynExpr.VAR)
                env.typ_orror(synt.e1.beg, synt.e1.end, "expected left value");
            e1 = new TypExpr(synt.e1, env, params, cl, scope, false, _static);
            e2 = new TypExpr(synt.e2, env, params, cl, scope, false, _static);
            if(!e2.type.isSubtypeOf(e1.type))
                env.typ_orror(e2.beg, e2.end, "this expression has type " + 
                        e2.type + " was expeted to be a subtype of " +
                        e1.type);
            type = e1.type;
            break;
        case SynExpr.UN_NOT:
            opType = NOT;
            e1 = new TypExpr(synt.e1, env, params, cl, scope, false, _static);
            if(!e1.type.isSame(TypType.bool_type))
                env.typ_orror(e1.beg, e1.end, "this expression has type " +
                        e1.type + " but was expected of type " +
                        TypType.bool_type);
            type = TypType.bool_type;
            break;
        case SynExpr.UN_MIN:
            opType = UMIN;
            e1 = new TypExpr(synt.e1, env, params, cl, scope, false, _static);
            if(!e1.type.isSame(TypType.int_type))
                env.typ_orror(e1.beg, e1.end, "this expression has type " +
                        e1.type + " but was expected of type " +
                        TypType.int_type);
            type = TypType.int_type;
            break;
        case SynExpr.OP:
            op = synt.op;
            e1 = new TypExpr(synt.e1, env, params, cl, scope, false, _static);
            e2 = new TypExpr(synt.e2, env, params, cl, scope, false, _static);
            opType = OP;
            switch(op){
            case Operator.EQ:
            case Operator.DIF:
                if(!e1.type.isSubtypeOf(e2.type) && !e2.type.isSubtypeOf(e1.type)){
                    env.typ_orror(beg, end, "operands are not of compatible types");
                    env.note_on_orror(e1.beg, e1.end, "first operand of type " + e1.type);
                    env.note_on_orror(e2.beg, e2.end, "second operand of type " + e2.type);
                }
                type = TypType.bool_type;
                break;
            case Operator.LESS:
            case Operator.LESSEQ:
            case Operator.GREATER:
            case Operator.GREATEREQ:
                if(!e1.type.isSame(TypType.int_type))
                    env.typ_orror(e1.beg, e1.end, "this expression has type " +
                            e1.type + " but was expected of type " +
                            TypType.int_type);
                if(!e2.type.isSame(TypType.int_type))
                    env.typ_orror(e2.beg, e2.end, "this expression has type " +
                            e2.type + " but was expected of type " +
                            TypType.int_type);
                type = TypType.bool_type;
                break;
            case Operator.SUB:
            case Operator.MUL:
            case Operator.DIV:
            case Operator.MOD:
                if(!e1.type.isSame(TypType.int_type))
                    env.typ_orror(e1.beg, e1.end, "this expression has type " +
                            e1.type + " but was expected of type " +
                            TypType.int_type);
                if(!e2.type.isSame(TypType.int_type))
                    env.typ_orror(e2.beg, e2.end, "this expression has type " +
                            e2.type + " but was expected of type " +
                            TypType.int_type);
                type = TypType.int_type;
                break;
            case Operator.AND:
            case Operator.OR:
                if(!e1.type.isSame(TypType.bool_type))
                    env.typ_orror(e1.beg, e1.end, "this expression has type " +
                            e1.type + " but was expected of type " +
                            TypType.bool_type);
                if(!e2.type.isSame(TypType.bool_type))
                    env.typ_orror(e2.beg, e2.end, "this expression has type " +
                            e2.type + " but was expected of type " +
                            TypType.bool_type);
                type = TypType.bool_type;
                break;
            case Operator.ADD:
                if(!e1.type.isSame(TypType.int_type) && 
                   !e1.type.isSame(TypType.str_type)){
                    type = TypType.orror_type;
                    env.typ_orror(e1.beg, e1.end, "this expression has type " +
                            e1.type + " but was expected of type " +
                            TypType.int_type + " or " + TypType.str_type);
                }
                if(!e2.type.isSame(TypType.int_type) && 
                   !e2.type.isSame(TypType.str_type)){
                    type = TypType.orror_type;
                    env.typ_orror(e2.beg, e2.end, "this expression has type " +
                            e2.type + " but was expected of type " +
                            TypType.int_type + " or " + TypType.str_type);
                }
                if(type == null)
                    type = e1.type.isSame(TypType.str_type) || 
                           e2.type.isSame(TypType.str_type) 
                                ? TypType.str_type : TypType.int_type;
                break;
            }
            break;
        case SynExpr.INT_LIT:
            opType = LIT_I;
            lit_i_val = synt.int_lit;
            type = TypType.int_type; 
            break;
        
        case SynExpr.BOOL_LIT:
            opType = LIT_B;
            lit_b_val = synt.bool_lit;
            type = TypType.bool_type; 
            break;
        
        case SynExpr.STR_LIT:
            opType = LIT_S;
            lit_s_val = synt.str_lit;
            type = TypType.str_type;
            break;
        
        case SynExpr.THIS:{
            opType = THIS;
            type = cl;
            if(_static){
                env.typ_orror(beg, end, "this is not defined in static context");
                type = TypType.orror_type; 
            }
            break;
        }
        case SynExpr.CONSTRUCT: {
            opType = BUILD;
            args = new Vector<TypExpr>();
            for(SynExpr e : synt.args)
                args.add(new TypExpr(e, env, params, cl, scope, false, _static));
            type = TypType.fromT(synt.nt, env, params);
            type.constr(env, args, beg, end);
            break;
        }
        case SynExpr.CALL: {
            opType = CALL;
            args = new Vector<TypExpr>();
            for(SynExpr e : synt.args)
                args.add(new TypExpr(e, env, params, cl, scope, false, _static));
            
            ident = synt.ident;
            if(synt.e1 == null){
                if(cl.getProtoStatic(ident, env) != null){
                    opType = STCALL;
                    from = new TypExpr(new SynExpr(beg, end, true), env, params, cl, scope, 
                            true, false);//from this
                    from.notExpr = true;
                    type = cl.retTypeOfStatic(ident, env, args, beg, end);
                }
                else{
                    from = new TypExpr(new SynExpr(beg, end, true), env, params, cl, scope, 
                            true, _static);
                    //from this
                    type = cl.retTypeOf(ident, env, args, beg, end);
                }
            } else{
                from = new TypExpr(synt.e1, env, params, cl, scope, true, _static);
                
                if(from.notExpr){
                    opType = STCALL;
                    type = from.type.retTypeOfStatic(ident, env, args, beg, end);
                } else
                    type = from.type.retTypeOf(ident, env, args, beg, end);
            }
            break;
        }
        case SynExpr.VAR:{
            ident = synt.ident;
            notExpr = false;
    
            if(synt.e1 == null){
                opType = VAR;
                if(scope != null)
                    type = scope.getVar(ident);
                
                if(type == null && !_static){
                    
                    opType = CLVAR;
                    from = new TypExpr(new SynExpr(beg, end, true), env, params, cl, 
                            scope, false, _static);
                    //from this
                    type = cl.hasField(ident, env);
                }
                    
                if(type == null){
                    opType = STVAR;
                    from = new TypExpr(new SynExpr(beg, end, true), env, params, cl, 
                            scope, false, false);
                    from.notExpr = true;
                    type = cl.hasFieldStatic(ident, env);
                }
    
                if(type == null && allowNoExpr){
                    notExpr = true;
                    type = TypType.fromT(new SynNtype(ident, null, beg, end), env, params);
                }
                
                if(type == null){
                    env.typ_orror(beg, end, "unknown identifier " + ident);
                    type = TypType.orror_type; 
                }
    
            } else {
                from = new TypExpr(synt.e1, env, params, cl, scope, true, _static);
                if(from.notExpr){
                    opType = STVAR;
                    type = from.type.typeOfStatic(ident, env, beg, end);
                } else {
                    opType = CLVAR;
                    type = from.type.typeOf(ident, env, beg, end);
                }
            }
            break;
            } 
        }
    }
}

