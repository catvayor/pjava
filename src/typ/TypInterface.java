package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypInterface{ 
    public SynClassIntf syn;
    
    public Vector<String> params_name;//for order
    public Hashtable<String, TypType> params;
    public Vector<TypType> exts;
    public Hashtable<String, TypProto> protos;

    public int arity(){
        return params_name.size();
    }

    public TypInterface(SynClassIntf ci){
        syn = ci;
        params_name = new Vector<String>();
        if(syn.paramstype == null)
            syn.paramstype = new Vector<SynParamtype>();
        for(SynParamtype pt : syn.paramstype)
            params_name.add(pt.ident);
    }

    public void build(TypEnvironment env){
        params = new Hashtable<String, TypType>();
        for(SynParamtype pt : syn.paramstype){
            TypType pc = params.get(pt.ident);
            if(pc == null)
                params.put(pt.ident, new TypType(pt.ident, pt.beg_decl, pt.end_decl));
            else{
                env.typ_orror(pt.beg_decl, pt.end_decl, pt.ident + " already defined");
                env.note_on_orror(pc.beg, pc.end, "first definition here");
            }
        }
        for(SynParamtype pt : syn.paramstype){
            Vector<TypType> exts = params.get(pt.ident).over_types;
            if(pt.ext_nt != null){//never empty if exist
                SynNtype nt = pt.ext_nt.get(0);
                TypType t = new TypType(nt, env, params);
                if(t.type != TypType.CLASS && t.type != TypType.PARAM){
                    exts.add(new TypType(TypEnvironment.OBJECT, env, params));
                }
                if(t.ident.equals("String"))
                    env.typ_orror(nt.beg, nt.end, "can't inherit from String");
                exts.add(t);
                for(int i = 1; i < pt.ext_nt.size(); ++i){
                    nt = pt.ext_nt.get(i);
                    t = new TypType(nt, env, params);
                    exts.add(t);
                    if(t.type != TypType.INTF)
                        env.typ_orror(nt.beg, nt.end, "expected interface");
                }
            }
        }

        exts = new Vector<TypType>();
        if(syn.exts != null)
            for(SynNtype nt : syn.exts){
                TypType t = new TypType(nt, env, params);
                exts.add(t);
                if(t.type != TypType.INTF)
                    env.typ_orror(nt.beg, nt.end, "expected interface");
            }
    }

    //same as Environment
    String check_inherit_impl(String id, Hashtable<String, Integer> currentState, 
                              TypEnvironment env){
        TypType t = params.get(id);
        Integer state = currentState.get(id);
        if(state.equals(1)){
            env.typ_orror(t.beg, t.end, "inheritance loop on " + id);
            return id;
        }
        if(state.equals(2))
            return null;

        currentState.put(id, 1);
        for(TypType ext : t.over_types){
            if(ext.type != TypType.PARAM)
                continue;
            String ret = check_inherit_impl(ext.ident, currentState, env);
            if(ret != null){
                env.note_on_orror(t.beg, t.end, "which is inherited by " + id);
                if(!ret.equals(id)){
                    currentState.put(id, 2);
                    return ret;
                }
            }
        }
        currentState.put(id, 2);
        return null;
    }
    public void check_arg_inheritance(TypEnvironment env){
        Hashtable<String, Integer> state = new Hashtable<String, Integer>();
        for(String id : params_name)
            state.put(id, 0);
        for(String id : params_name)
            check_inherit_impl(id, state, env);
    }

    public void build_over_types(TypEnvironment env){
        for(TypType t : exts)
            t.build_over_types(env);
        for(TypType t : params.values())
            t.build_over_types(env);
    }

    public Hashtable<String, TypType> substTable(Vector<TypType> spec){
        //spec and params assumed of same size
        Hashtable<String, TypType> ret = new Hashtable<String, TypType>();
        for(int i = 0; i < arity(); ++i)
            ret.put(params_name.get(i), spec.get(i));
        return ret;
    }
    public void checkValid(TypEnvironment env){
        for(TypType t : exts)
            t.checkValid(env);
        for(TypType t : params.values())
            for(TypType p : t.over_types)
                p.checkValid(env);
    }

    public void buildProtos(TypEnvironment env){
        protos = new Hashtable<String, TypProto>();
        for(SynProto p : syn.protos){
            TypProto pc = protos.get(p.ident);
            if(pc != null){
                env.typ_orror(p.beg, p.end, 
                        "prototype " + p.ident + " already defined");
                env.note_on_orror(pc.beg, pc.end,
                        "first definition here");
            } 
            else
                protos.put(p.ident, new TypProto(p, env, params));
        }
    }

    public void checkProtosConflict(TypEnvironment env){
        SynNtype ment = new SynNtype(syn.ident, null, syn.beg_decl, syn.end_decl);
        if(!params_name.isEmpty()){
            ment.gen_param = new Vector<SynNtype>();
            for(String pid : params_name)
                ment.gen_param.add(new SynNtype(pid, null, syn.beg_decl, syn.end_decl));
        }
        TypType me = TypType.fromT(ment, env, params);
       
        Hashtable<String, TypProto> eff_protos = me.checkImplementsConflict(env);

        for(TypType param : params.values())
            param.checkImplementsConflict(env);

        for(String mid : protos.keySet()){
            TypProto ext_p = eff_protos.get(mid);
            TypProto p = protos.get(mid);

            if(ext_p == null)
                continue;

            ext_p.checkSpecialisation(env, p);
        }
    }
}
