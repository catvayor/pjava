package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypMethod extends TypProto{
    SynMethod syn;
    public TypScope body;

    public TypMethod(SynMethod m, TypEnvironment env, Hashtable<String, TypType> params){
        super(m.proto, env, params);
        syn = m;
    }

    public void buildBody(TypEnvironment env, Hashtable<String, TypType> params, TypType cl){
        body = new TypScope(syn.body, env, params, cl, 
                super.retType, super.arguments, super._static);
        if(!super.retType.isSame(TypType.void_type))
            if(!body.returns){
                env.typ_orror(super.beg, super.end,
                        "non void function should return in every control flow");
            }
    }
}
