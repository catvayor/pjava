package typ;
import java.util.Vector;
import java.util.Hashtable;
import global.*;
import syn.*;

public class TypProto{
    public boolean isOrror;

    public static final TypProto orror_proto = new TypProto(true);

    public Location beg;
    public Location end;

    public TypType retType;
    public Vector<Pair<String, TypType> > arguments;
    public String ident;
    public boolean _public;
    public boolean _static;

    public TypProto(SynProto p, TypEnvironment env, Hashtable<String, TypType> params){
        isOrror = false;
        beg = p.beg;
        end = p.end;
        _public = p._public;
        _static = p._static;
        ident = p.ident;
        retType = TypType.fromT(p.ret, env, params);
        
        arguments = new Vector<Pair<String, TypType> >();
        Hashtable<String, SynParameter> arg_params = new Hashtable<String, SynParameter>();
        for(SynParameter arg : p.params){
            arguments.add(new Pair<String, TypType>(
                        arg.ident,
                        TypType.fromT(arg.type, env, params)));
            SynParameter param = arg_params.get(arg.ident);
            if(param != null){
                env.typ_orror(arg.beg, arg.end, arg.ident + " already defined");
                env.typ_orror(param.beg, param.end, "first definition here");
            }
            arg_params.put(arg.ident, arg);
        }
    }

    TypProto(boolean o){ isOrror = o; }

    public TypProto substitution(Hashtable<String, TypType> subst){
        TypProto p = new TypProto(false);
        p.beg = beg;
        p.end = end;
        p._public = _public;
        p._static = _static;
        p.ident = ident;
        p.retType = retType.substitution(subst);
        p.arguments = new Vector<Pair<String, TypType> >();
        for(Pair<String,TypType> arg : arguments)
            p.arguments.add(
                new Pair<String, TypType>(arg.first, arg.second.substitution(subst)));
        return p;
    }

    public void checkSpecialisation(TypEnvironment env, TypProto other){
        if(isOrror || other.isOrror)
            return;

        if(!other.retType.isSubtypeOf(retType)){
            env.typ_orror(other.beg, other.end, 
                    "invalid prototype specialization of " + ident);
            env.note_on_orror(other.retType.beg, other.retType.end, 
                    "expected return type " + retType + " got " + other.retType);
            env.note_on_orror(beg, end, "specialized prototype");
        }

        if(other.arguments.size() != arguments.size()){
            env.typ_orror(other.beg, other.end, 
                    "invalid prototype specialization of " + ident);
            env.note_on_orror(other.beg, other.end, "wrong number of arguments");
            env.note_on_orror(beg, end, "specialized prototype");
            return;
        }
        for(int i = 0; i < arguments.size(); ++i){
            if(!arguments.get(i).second.isSame(
                        other.arguments.get(i).second)){
                env.typ_orror(other.beg, other.end, 
                        "invalid prototype specialization of " + ident);
                env.note_on_orror(other.arguments.get(i).second.beg, 
                                  other.arguments.get(i).second.end, "expected type " +
                                  arguments.get(i).second + " for argument " +
                                  other.arguments.get(i).first + " got " + 
                                  other.arguments.get(i).second);
                env.note_on_orror(beg, end, 
                        "specialized prototype, argument is " +
                        arguments.get(i).first);
            }
        }
    }
    
    public boolean isSpecialisation(TypProto other){
        if(isOrror || other.isOrror)
            return true;

        if(!other.retType.isSubtypeOf(retType))
            return false;

        if(other.arguments.size() != arguments.size())
            return false;

        for(int i = 0; i < arguments.size(); ++i)
            if(!arguments.get(i).second.isSame(
                        other.arguments.get(i).second))
                return false;

        return true;
    }
}
