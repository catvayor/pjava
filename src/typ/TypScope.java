package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypScope{
    public Hashtable<String, TypType> local_vars;
    public Hashtable<String, TypType> paren_vars;
    public Hashtable<String, Integer> var_loc;
    public int depth;
    public int max_depth;

    public Vector<TypStatement> stmts;
    public boolean returns;

    public TypType getVar(String id){
        TypType ret = local_vars.get(id);
        if(ret != null)
            return ret;
        return paren_vars.get(id);
    }

    public TypType addVar(String id, TypType ty){
        TypType ret = getVar(id);
        local_vars.put(id,ty);
        depth -= 8;
        var_loc.put(id,depth);
        considerDepth(depth);
        return ret;
    }

    public void considerDepth(int d){
        max_depth = d < max_depth? d: max_depth;
    }

    public TypScope(Vector<SynStatement> syns, TypEnvironment env, 
            Hashtable<String, TypType> params, TypType cl, TypType retT, 
            TypScope parent, boolean loop, boolean swtch, boolean _static){
        returns = false;
        depth = parent.depth;
        max_depth = parent.max_depth;
        local_vars = new Hashtable<String, TypType>();
        paren_vars = new Hashtable<String, TypType>(parent.paren_vars);
        paren_vars.putAll(parent.local_vars);
        var_loc = new Hashtable<String, Integer>(parent.var_loc);

        stmts = new Vector<TypStatement>();
        for(SynStatement syn : syns){
            TypStatement stmt = new TypStatement(syn, env, params, cl, retT, this, 
                    false, loop, swtch, _static);
            stmts.add(stmt);
            returns = returns || stmt.returns;
        }
    }
    
    public TypScope(Vector<SynStatement> syns, TypEnvironment env, 
            Hashtable<String, TypType> params, TypType cl, TypType retT, 
            Vector<Pair<String,TypType> > args, boolean _static){
        returns = false;
        depth = 16 + 8*args.size();
        if(!_static)
            depth += 8;
        local_vars = new Hashtable<String, TypType>();
        paren_vars = local_vars;
        var_loc = new Hashtable<String, Integer>();
        for(Pair<String, TypType> arg : args)
            addVar(arg.first, arg.second);
        local_vars = new Hashtable<String, TypType>();
        depth = 0;
        max_depth = 0;

        stmts = new Vector<TypStatement>();
        for(SynStatement syn : syns){
            TypStatement stmt = new TypStatement(syn, env, params, cl, retT, this,
                    false, false, false, _static);
            stmts.add(stmt);
            returns = returns || stmt.returns;
        }
    }
}
