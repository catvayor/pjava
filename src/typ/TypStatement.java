package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypStatement{
    public static final int NOP    = 0;
    public static final int EVAL   = 1;
    public static final int DECL   = 2;
    public static final int IF     = 3;
    public static final int WHILE  = 4;
    public static final int BLOCK  = 5;
    public static final int RET    = 6;

    public static final int CONT   = 7;
    public static final int BRK    = 8;
    public static final int FOR    = 9;
    public static final int FORIT  = 10;
    public static final int SWITCH = 11;

    public int s_type;

    public boolean returns;

    public TypExpr expr;
    public TypType ty;
    public String ident;

    public TypStatement s_true;
    public TypStatement s_false;
    public TypScope body;
    public TypScope bodyElse;

    public TypExpr forInit;
    public TypExpr forInc;

    public Vector<Pair<TypExpr,Vector<TypStatement>>> cases;
    public Vector<TypStatement> defCase;

    TypStatement() { returns = false; s_type = NOP; }

    public TypStatement(SynStatement synt, TypEnvironment env,
            Hashtable<String, TypType> params, TypType cl, TypType retT, 
            TypScope scope, boolean noDecl, boolean loop, boolean swtch, boolean _static){
        returns = false;
        switch(synt.type){
        case SynStatement.EMPTY:
            s_type = NOP;
            break;
        case SynStatement.EVAL:
            s_type = EVAL;
            expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
            break;
        case SynStatement.DECL:
            s_type = DECL;
            if(noDecl)
                env.typ_orror(synt.beg, synt.end, "no declaration alowed here");

            ident = synt.ident;
            ty = scope.addVar(synt.ident, TypType.fromT(synt.t, env, params));
            if(ty != null){
                env.typ_orror(synt.beg, synt.end, ident + " already defined");
                env.note_on_orror(ty.beg, ty.end, "first definition here");
            }
            ty = scope.getVar(synt.ident);
            if(synt.e != null){
                expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
                if(!expr.type.isSubtypeOf(ty))
                    env.typ_orror(expr.beg, expr.end, "expected expression of type " + ty +
                                                      " got " + expr.type);
            }
            break;
        case SynStatement.IF:
            s_type = IF;
            expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
            if(!expr.type.isSame(TypType.bool_type))
                env.typ_orror(expr.beg, expr.end, "expected expression of type " + 
                                                  TypType.bool_type + " got " + expr.type);
            body = new TypScope(new Vector<SynStatement>(), env, params,
                                         cl, retT, scope, loop, swtch, _static);
            s_true = new TypStatement(synt.body, env, params, cl, retT, body, 
                    noDecl, loop, swtch, _static);
            scope.considerDepth(body.max_depth);
            if(synt.belse != null){
                bodyElse = new TypScope(new Vector<SynStatement>(), env, params,
                                            cl, retT, scope, loop, swtch, _static);
                s_false = new TypStatement(synt.belse, env, params, cl, retT, bodyElse, 
                        noDecl, loop, swtch, _static);
                scope.considerDepth(bodyElse.max_depth);
            }
            else
                s_false = new TypStatement();
            returns = s_true.returns && s_false.returns;
            break;
        case SynStatement.WHILE:
            s_type = WHILE;
            expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
            if(!expr.type.isSame(TypType.bool_type))
                env.typ_orror(expr.beg, expr.end, "expected expression of type " + 
                                                  TypType.bool_type + " got " + expr.type);
            body = new TypScope(new Vector<SynStatement>(), env, params,
                                         cl, retT, scope, true, false, _static);
            s_true = new TypStatement(synt.body, env, params, cl, retT, body, 
                    noDecl, true, false, _static);
            scope.considerDepth(body.max_depth);
            break;
        case SynStatement.BLOCK:
            s_type = BLOCK;
            body = new TypScope(synt.stmts, env, params, cl, retT, scope, loop, swtch, _static);
            scope.considerDepth(body.max_depth);
            returns = body.returns;
            break;
        case SynStatement.RETURN:
            s_type = RET;
            returns = true;
            if(synt.e == null){
                if(!retT.isSame(TypType.void_type))
                    env.typ_orror(synt.beg, synt.end, 
                            "non void functions must return expressions");
            } else {
                expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
                if(!expr.type.isSubtypeOf(retT))
                    env.typ_orror(expr.beg, expr.end, "expected expression of type " + 
                                                      retT + " got " + expr.type);
            }
            break;
        case SynStatement.CONTINUE:
            s_type = CONT;
            if(!loop)
                env.typ_orror(synt.beg, synt.end, "can continue only in loop");
            break;
        case SynStatement.BREAK:
            s_type = BRK;
            if(!loop && !swtch)
                env.typ_orror(synt.beg, synt.end, "can break only in switch or loop");
            break;
        case SynStatement.FORIT:
            s_type = FORIT;
            ty = TypType.fromT(synt.t, env, params);
            ident = synt.ident;
            expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
            {
                TypScope varScope = new TypScope(new Vector<SynStatement>(), env, params,
                                                 cl, retT, scope, true, false, _static);
                TypType t = varScope.addVar(ident, ty);
                if(t != null){
                    env.typ_orror(ty.beg, ty.end, ident + " already defined");
                    env.note_on_orror(t.beg, t.end, "first definition here");
                }
                s_true = new TypStatement(synt.body, env, params, cl, retT, varScope, 
                        noDecl, true, false, _static);
                body = varScope;
                scope.considerDepth(varScope.max_depth);
            }
            if(expr.type.isArray()){
                if(!expr.type.arrT.isSubtypeOf(ty))
                    env.typ_orror(expr.beg, expr.end, "expected array of " + ty + 
                                    ", got array of " + expr.type.arrT);
            } else{//iterable checking 
                TypType itType = expr.type.retTypeOf("iterator", env, 
                        new Vector<TypExpr>(), expr.type.beg, expr.type.end);
                TypType hasNextT = itType.retTypeOf("hasNext", env,
                        new Vector<TypExpr>(), expr.type.beg, expr.type.end);
                TypType nxtT = itType.retTypeOf("next", env,
                        new Vector<TypExpr>(), expr.type.beg, expr.type.end);

                if(!hasNextT.isSame(TypType.bool_type, params))
                    env.typ_orror(expr.beg, expr.end, "expected return type of hasNext for " + 
                            itType + " is " + TypType.bool_type + " but is " + hasNextT);
                
                if(!nxtT.isSubtypeOf(ty, params))
                    env.typ_orror(expr.beg, expr.end, "expected return type of next for " + 
                            itType + " is subtype of " + ty + " but is " + nxtT);
            }
            break;
        case SynStatement.FOR:
            s_type = FOR;
            ty = TypType.fromT(synt.t, env, params);
            ident = synt.ident;
            forInit = new TypExpr(synt.init, env, params, cl, scope, false, _static);
            if(!forInit.type.isSubtypeOf(ty))
                env.typ_orror(forInit.beg, forInit.end, "expected expression of type " +
                                                        ty + " got " + forInit.type);
            {
                TypScope varScope = new TypScope(new Vector<SynStatement>(), env, params,
                                                 cl, retT, scope, true, false, _static);
                TypType t = varScope.addVar(ident, ty);
                if(t != null){
                    env.typ_orror(ty.beg, ty.end, ident + " already defined");
                    env.note_on_orror(t.beg, t.end, "first definition here");
                }
            
                expr = new TypExpr(synt.e, env, params, cl, varScope, false, _static);
                if(!expr.type.isSame(TypType.bool_type, params))
                    env.typ_orror(expr.beg, expr.end, "expected expression of type " + 
                                                      TypType.bool_type + " got " + expr.type);
                
                forInc = new TypExpr(synt.inc, env, params, cl, varScope, false, _static);
            
                s_true = new TypStatement(synt.body, env, params, cl, retT, varScope, 
                        noDecl, true, false, _static);

                body = varScope;
                scope.considerDepth(varScope.max_depth);
            }
            break;
        case SynStatement.SWITCH:
            s_type = SWITCH;
            expr = new TypExpr(synt.e, env, params, cl, scope, false, _static);
            cases = new Vector<Pair<TypExpr,Vector<TypStatement>>>();
            for(Pair<SynExpr,Vector<SynStatement>> ca : synt.cases){
                Vector<TypStatement> stmts = new Vector<TypStatement>();
                for(SynStatement stmt : ca.second)
                    stmts.add(new TypStatement(stmt, env, params, cl, retT, scope, 
                                true, loop, true, _static));//loop or false?
                TypExpr c = new TypExpr(ca.first, env, params, cl, scope, false, _static);
                if(!c.type.isSubtypeOf(expr.type) && !expr.type.isSubtypeOf(c.type)){
                    env.typ_orror(c.beg, c.end, "operands are not of compatible types");
                    env.note_on_orror(expr.beg, expr.end, "first operand of type " + expr.type);
                    env.note_on_orror(c.beg, c.end, "second operand of type " + c.type);
                }
                cases.add(new Pair<TypExpr, Vector<TypStatement>>(c,stmts));
            }
            defCase = new Vector<TypStatement>();
            for(SynStatement stmt : synt.defaultCase)
                defCase.add(new TypStatement(stmt, env, params, cl, retT, scope, 
                            true, loop, true, _static));//loop or false?
            break;
        }
    }
}
