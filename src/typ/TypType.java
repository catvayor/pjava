package typ;
import java.util.Vector;
import java.util.Hashtable;
import syn.*;
import global.*;

public class TypType{
    static final int VOID     = 0;
    static final int BOOLEAN  = 1;
    static final int INT      = 2;
    static final int TYPENULL = 3;
    static final int ARRT     = 4;

    static final int PARAM = -1;
    static final int CLASS = -2;
    static final int INTF  = -3;

    static final int ORROR = -4;

    public Location beg;
    public Location end;

    public int type;

    public String ident;
    public Vector<TypType> params;
    public Vector<TypType> over_types;
    public TypType arrT;
    public int over_built;//0 no, 1 in prog, 2 done;

    public static TypType orror_type = new TypType(ORROR, 
                    new Location(-1,-1), new Location(-1,-1));

    public static TypType int_type = new TypType(INT, 
                    new Location(-1,-1), new Location(-1,-1));
    
    public static TypType bool_type = new TypType(BOOLEAN, 
                    new Location(-1,-1), new Location(-1,-1));

    public static TypType str_type = null;

    public static TypType null_type = new TypType(TYPENULL, 
                    new Location(-1,-1), new Location(-1,-1));
    
    public static TypType void_type = new TypType(VOID, 
                    new Location(-1,-1), new Location(-1,-1));

    public TypType(SynNtype nt, TypEnvironment env, Hashtable<String, TypType> env_params){
        over_built = 0;
        beg = nt.beg;
        end = nt.end;
        ident = nt.ident;
        if(env_params.containsKey(ident)){
            type = PARAM;
            over_types = env_params.get(ident).over_types;
            if(nt.gen_param != null){
                env.typ_orror(beg, end, "unexpected type parameter");
                type = ORROR;
            }
        } 
        else if(env.interfaces.containsKey(ident)){
            type = INTF;
            TypInterface intf = env.interfaces.get(ident);
            params = new Vector<TypType>();
            if(nt.gen_param != null)
                for(SynNtype param : nt.gen_param)
                    params.add(new TypType(param, env, env_params));
            if(params.size() != intf.arity()){
                env.typ_orror(beg, end, 
                        "wrong number of type parameter : expected " + 
                        intf.arity() + ", got " + params.size());
                type = ORROR;
            }
            over_types = new Vector<TypType>();
        } 
        else if(env.classes.containsKey(ident)){
            type = CLASS;
            TypClass c = env.classes.get(ident);
            params = new Vector<TypType>();
            if(nt.gen_param != null)
                for(SynNtype param : nt.gen_param)
                    params.add(new TypType(param, env, env_params));
            if(params.size() != c.arity()){
                env.typ_orror(beg, end, 
                        "wrong number of type parameter : expected " + 
                        c.arity() + ", got " + params.size());
                type = ORROR;
            }
            over_types = new Vector<TypType>();
        } 
        else {
            env.typ_orror(beg, end, "unknown type " + ident);
            type = ORROR;
        }
    }

    public TypType(String id, Location b, Location e){
        over_built = 0;
        beg = b;
        end = e;
        type = PARAM;
        ident = id;
        over_types = new Vector<TypType>();
    }

    public String toString(){
        switch(type){
            case VOID: return "void";
            case INT: return "int";
            case BOOLEAN: return "boolean";
            case TYPENULL: return "<type of null>";
            case ARRT: return arrT.toString() + "[]";
            case PARAM: return ident;
            case INTF:
            case CLASS:{
                String ret = ident;
                if (params.isEmpty())
                    return ret;
                ret += "<" + params.get(0).toString();
                for(int i = 1; i < params.size(); ++i)
                    ret += ", " + params.get(i).toString();
                return ret + " >";
            }
            case ORROR: return "<error type (created by a typing error)>";
            default: return "<UNKNOWN TYPE ERROR>";
        }
    }

    public boolean isArray(){
        return type == ARRT;
    }

    public static TypType fromT(SynType t, TypEnvironment env, 
            Hashtable<String, TypType> env_params){
        switch(t.type){
            case SynType.NTYPE:
                return fromT(t.nt, env, env_params);
            case SynType.BOOL: return new TypType(BOOLEAN, t.beg, t.end);
            case SynType.INT:  return new TypType(INT, t.beg, t.end);
            case SynType.VOID: return new TypType(VOID, t.beg, t.end);
            case SynType.ARR:  return new TypType(fromT(t._t,env,env_params),t.beg,t.end, env);
            default:{
                env.internal_orror(t.beg, t.end, "encountered unknown Type type " + t.type);
                return new TypType(ORROR, t.beg, t.end);
            }
        }
    }

    public static TypType fromT(SynNtype nt, TypEnvironment env, 
            Hashtable<String, TypType> env_params){
        TypType ret = new TypType(nt, env, env_params);
        ret.build_over_types(env);
        ret.checkValid(env);
        return ret;
    }

    TypType(int code, Location b, Location e, String i, Vector<TypType> p, Vector<TypType> o){
        type = code;
        beg = b;
        end = e;
        ident = i;
        params = p;
        over_types = o;
        over_built = 2;//?
    }

    TypType(int code, Location b, Location e){
        over_built = 2;
        type = code;
        beg = b;
        end = e;
    }
    
    TypType(TypType t, Location b, Location e, TypEnvironment env){
        over_built = 2;
        type = ARRT;
        arrT = t;
        if(t.isSame(void_type))
            env.typ_orror(b, e, "cannot declare array of void");
        beg = b;
        end = e;
    }

    public TypType substitution(Hashtable<String, TypType> subst){
        switch(type){
            case PARAM:{
                TypType ty = subst.get(ident);
                if(ty != null)
                    return ty;
                Vector<TypType> o = new Vector<TypType>();
                for(TypType t : over_types)
                    o.add(t.substitution(subst));
                return new TypType(PARAM, beg, end, ident, null, o);
            }
            case CLASS:{//specialization suposed correct
                Vector<TypType> o = new Vector<TypType>();
                for(TypType t : over_types)
                    o.add(t.substitution(subst));
                Vector<TypType> parameter = new Vector<TypType>();
                for(TypType t : params)
                    parameter.add(t.substitution(subst));
                return new TypType(CLASS, beg, end, ident, parameter, o);
            }
            case INTF:{//specialization suposed correct
                Vector<TypType> o = new Vector<TypType>();
                for(TypType t : over_types)
                    o.add(t.substitution(subst));
                Vector<TypType> parameter = new Vector<TypType>();
                for(TypType t : params)
                    parameter.add(t.substitution(subst));
                return new TypType(INTF, beg, end, ident, parameter, o);
            }
            default:
                return this;
        }
    }

    public void build_over_types(TypEnvironment env){
        if(over_built != 0)
            return;
        if(type == ARRT){
            arrT.build_over_types(env);
            return;
        }
        if(type != PARAM && type != CLASS && type != INTF)
            return;

        over_built = 1;

        //solve Object cycle by destroying it
        if(ident.equals("Object")){
            over_types.clear();
            over_built = 2;
            return;
        }

        for(TypType t : over_types)
            t.build_over_types(env);

        if(type == PARAM){
            over_built = 2;
            return;
        }
        
        for(TypType t : params)
            t.build_over_types(env);
        
        if(type == INTF){
            over_types.add(new TypType(TypEnvironment.OBJECT, env, 
				new Hashtable<String,TypType>()));
            TypInterface intf = env.interfaces.get(ident);
            Hashtable<String, TypType> substTable = intf.substTable(params);
            for(TypType t : intf.exts){
                t.build_over_types(env);
                over_types.add(t.substitution(substTable));
            }
        } else if(type == CLASS){
            TypClass c = env.classes.get(ident);
            Hashtable<String, TypType> substTable = c.substTable(params);
            over_types = new Vector<TypType>();
            c.ext.build_over_types(env);
            over_types.add(c.ext.substitution(substTable));
            for(TypType t : c.impls){
                t.build_over_types(env);
                over_types.add(t.substitution(substTable));
            }
        }
        over_built = 2;
    }

    public boolean isSame(TypType other){
        return isSame(other, new Hashtable<String, TypType>());
    }

    public boolean isSame(TypType other, Hashtable<String, TypType> subst){
        if(type == ORROR || other.type == ORROR)
            return true;

        if(other.type == PARAM){
            TypType t = subst.get(other.ident);
            if (t==null)
                return (type == PARAM) && ident.equals(other.ident);
            return isSame(t);
        }

        if(other.type != type)
            return false;
        if(type == ARRT)
            return arrT.isSame(other.arrT, subst);
        if(type != CLASS && type != INTF)//basic type
            return true;
        if(!ident.equals(other.ident))
            return false;
        
        for(int i = 0; i < params.size(); ++i)
            //arity are correct and same classIntf : both params of same size
            if(!params.get(i).isSame(other.params.get(i), subst))
                return false;
        
        return true;
    }

    public boolean isSubtypeOf(TypType other){
        return isSubtypeOf(other, new Hashtable<String, TypType>());
    }

    public boolean isSubtypeOf(TypType other, Hashtable<String, TypType> subst){
        if(isSame(other, subst))
            return true;//t subtype of t

        if(type == TYPENULL)
            return other.type == PARAM ||
                   other.type == CLASS ||
                   other.type == INTF  ||
                   other.type == ARRT;

        if(type == ARRT && other.type == ARRT)
            return arrT.isSubtypeOf(other.arrT, subst);

        if(type != PARAM && type != CLASS && type != INTF)
            //basic type has no other over type != themselves
            return false;
        
        for(TypType over : over_types)
            if(over.isSubtypeOf(other, subst))
                return true; //subtyping is transitive

        return false;
    }

    public void checkValid(TypEnvironment env){
        if(type == ARRT)
            arrT.checkValid(env);
        if(type != CLASS && type != INTF)
            return;

        for(TypType t : params)
            t.checkValid(env);

        Hashtable<String, TypType> subst;
        Hashtable<String, TypType> parameters;

        if(type == CLASS){
            TypClass c = env.classes.get(ident);
            subst = c.substTable(params);
            parameters = c.params;
        }
        else{
            TypInterface intf = env.interfaces.get(ident);
            subst = intf.substTable(params);
            parameters = intf.params;
        }
        
        for(String param : subst.keySet())
            for(TypType t : parameters.get(param).over_types){
                TypType param_giv = subst.get(param);
                if(!param_giv.isSubtypeOf(t, subst)){
                    env.typ_orror(beg, end, "invalid type");
                    env.note_on_orror(param_giv.beg, param_giv.end, 
                            "this parameter don't complies with requirements");
                    env.note_on_orror(t.beg, t.end, "requirements defined here");
                    //type = ORROR; ?
                }
            }
    }

    TypProto getProto(String id, TypEnvironment env){
        if(type == ORROR)
            return TypProto.orror_proto;

        TypClass c = env.classes.get(ident);
        if(c != null){
            TypMethod m = c.methods.get(id);
            if(m != null){
                Hashtable<String, TypType> subst = c.substTable(params);
                return m.substitution(subst);
            }
        }
        
        TypInterface intf = env.interfaces.get(ident);
        if(intf != null){
            TypProto p = intf.protos.get(id);
            if(p != null){
                Hashtable<String, TypType> subst = intf.substTable(params);
                return p.substitution(subst);
            }
        }

        for(TypType t : over_types){
            TypProto ret = t.getProto(id, env);
            if(ret != null)
                return ret;
        }

        return null;
    }
    
    TypProto getProtoStatic(String id, TypEnvironment env){
        if(type == ORROR)
            return TypProto.orror_proto;
        
        if(type != CLASS)
            return null;

        TypClass c = env.classes.get(ident);
        TypMethod m = c.static_methods.get(id);
            if(m != null){
            Hashtable<String, TypType> subst = c.substTable(params);
            return m.substitution(subst);
        }
        
        return null;
    }

    public TypType retTypeOf(String id, TypEnvironment env, Vector<TypExpr> args,
            Location beg, Location end){
        TypProto p = getProto(id, env);
        if (p == null){
            env.typ_orror(beg, end, "unknow function " + id);
            return orror_type;
        }
        
        if(p.isOrror)
            return orror_type;

        if(args.size() != p.arguments.size()){
            env.typ_orror(beg, end, "wrong number of argument : expected " +
                        p.arguments.size() + ", got " + args.size());
            return p.retType;
        }

        for(int i = 0; i < args.size(); ++i)
            if(!args.get(i).type.isSubtypeOf(p.arguments.get(i).second))
                env.typ_orror(args.get(i).beg, args.get(i).end,
                        "expected " + p.arguments.get(i).second + " for argument " +
                        p.arguments.get(i).first + ", got " + args.get(i).type);

        return p.retType;
    }
    
    public TypType retTypeOfStatic(String id, TypEnvironment env, Vector<TypExpr> args,
            Location beg, Location end){
        TypProto p = getProtoStatic(id, env);
        if (p == null){
            env.typ_orror(beg, end, "unknow function " + id);
            return orror_type;
        }
        
        if(p.isOrror)
            return orror_type;

        if(args.size() != p.arguments.size()){
            env.typ_orror(beg, end, "wrong number of argument : expected " +
                        p.arguments.size() + ", got " + args.size());
            return p.retType;
        }

        for(int i = 0; i < args.size(); ++i)
            if(!args.get(i).type.isSubtypeOf(p.arguments.get(i).second))
                env.typ_orror(args.get(i).beg, args.get(i).end,
                        "expected " + p.arguments.get(i).second + " for argument " +
                        p.arguments.get(i).first + ", got " + args.get(i).type);

        return p.retType;
    }
    
    public TypType typeOf(String id, TypEnvironment env, Location beg, Location end){
        TypType r = hasField(id, env);
        if(r == null){
            env.typ_orror(beg, end, id + " is not a field of " + ident + 
                    " or of it's base class");
            return orror_type;
        }
        return r;
    }
    
    public TypType typeOfStatic(String id, TypEnvironment env, Location beg, Location end){
        TypType r = hasFieldStatic(id, env);
        if(r == null){
            env.typ_orror(beg, end, id + " is not a static field of " + ident);
            return orror_type;
        }
        return r;
    }

    public TypType hasField(String id, TypEnvironment env){
        if(type == ORROR)
            return orror_type;

        TypClass c = env.classes.get(ident);
        if(c != null){
            TypType t = c.fields.get(id);
            if(t != null){
                Hashtable<String, TypType> subst = c.substTable(params);
                return t.substitution(subst);
            }
        }

        for(TypType t : over_types){//here just first one is enough
            TypType ret = t.hasField(id, env);
            if(ret != null)
                return ret;
        }

        return null;
    }
    
    public TypType hasFieldStatic(String id, TypEnvironment env){
        if(type == ORROR)
            return orror_type;
        if(type != CLASS)
            return null;

        TypClass c = env.classes.get(ident);
        TypType t = c.static_fields.get(id);
        if(t != null){
            Hashtable<String, TypType> subst = c.substTable(params);
            return t.substitution(subst);
        }

        return null;
    }

    public void constr(TypEnvironment env, Vector<TypExpr> args, Location b, Location e){
        if(type == ORROR)
            return;
        if(type != CLASS){
            env.typ_orror(b, e, "only class provide constructor");
            return;
        }
        TypClass cl = env.classes.get(ident);
        TypConstructor c = cl.constructor;
        
        if(args.size() != c.arguments.size()){
            env.typ_orror(beg, end, "wrong number of argument : expected " +
                        c.arguments.size() + ", got " + args.size());
            return;
        }

        for(int i = 0; i < args.size(); ++i)
            if(!args.get(i).type.isSubtypeOf(c.arguments.get(i).second, 
                                cl.substTable(params)))
                env.typ_orror(args.get(i).beg, args.get(i).end,
                        "expected " + c.arguments.get(i).second + " for argument " +
                        c.arguments.get(i).first + ", got " + args.get(i).type);
    }

    public Vector<Pair<String, TypProto> > listIntfProtos(TypEnvironment env){
        if(type != INTF)
            return new Vector<Pair<String, TypProto> >();

        Vector<Pair<String, TypProto> > protos = new Vector<Pair<String, TypProto> >();
        for(TypType t : over_types)
            protos.addAll(t.listIntfProtos(env));
        
        TypInterface intf = env.interfaces.get(ident);
        for(String pid : intf.protos.keySet())
            protos.add(new Pair<String, TypProto>(
                        pid, intf.protos.get(pid).substitution(intf.substTable(params))
                        ));

        return protos;
    }

    public Hashtable<String, TypProto> listClProtos(TypEnvironment env){
        if(type != CLASS)
            return new Hashtable<String, TypProto>();

        Hashtable<String, TypProto> protos =
            over_types.isEmpty() ? new Hashtable<String, TypProto>()
                                 : over_types.get(0).listClProtos(env);
        
        TypClass c = env.classes.get(ident);
        for(String pid : c.methods.keySet())
            protos.put(pid, c.methods.get(pid).substitution(c.substTable(params)));
        return protos;
    }
    
    public Hashtable<String,TypProto> checkImplementsConflict(TypEnvironment env){
        Hashtable<String, TypProto> eff_protos = new Hashtable<String, TypProto>();

        for(TypType ext : over_types){
            Vector<Pair<String, TypProto>> ext_ps = ext.listIntfProtos(env);
            for(Pair<String, TypProto> op : ext_ps){
                String pid = op.first;
                TypProto ext_p = op.second;
                TypProto p = eff_protos.get(pid);
                if(p == null){
                    eff_protos.put(pid, ext_p);
                    continue;
                }
                
                if(ext_p.isSpecialisation(p)) 
                    continue;
                if(p.isSpecialisation(ext_p)){
                    eff_protos.put(pid, ext_p);
                    continue;
                }

                env.typ_orror(beg, end, 
                        "uncompatible interfaces on prototype " + pid);
                env.note_on_orror(p.beg, p.end, "this prototype is not compatible with");
                env.note_on_orror(ext_p.beg, ext_p.end, "this prototype");
                env.note_on_orror(beg, end,
                        "both are included, directly or not, in " + ident);
                eff_protos.put(pid, TypProto.orror_proto);
            } 
        }
        return eff_protos;
    }

    public int getFieldLoc(String id, TypEnvironment env){
        TypClass c = env.classes.get(ident);
        return c.fields_idx.get(id);
    }
    
    public int getClSize(TypEnvironment env){
        TypClass c = env.classes.get(ident);
        return c.size;
    }

    public TypScope getConstrScope(TypEnvironment env){
        TypClass c = env.classes.get(ident);
        return c.constructor.body;
    }
}
